#ifndef __BioSim__Renderer__
#define __BioSim__Renderer__

#include "opengl.h"

#include <glm/glm.hpp>

class Simulator;

class Renderer
{
public:
	/**
	 * Constructs a new \c Renderer.
	 */
	Renderer();
	
	/**
	 * Initializes the OpenGL resources for the renderer. A valid OpenGL
	 * context must be active.
	 *
	 * @param simulator  The simulator to render.
	 */
	bool initialize(Simulator *simulator);
	
	/**
	 * Reshapes the viewport. The OpenGL context must be active.
	 *
	 * @param width   The viewport width.
	 * @param height  The viewport height.
	 */
	void reshape(int width, int height);
	
	/**
	 * The main drawing function. The OpenGL context must be active.
	 *
	 * @param delta  The time delta.
	 */
	void draw();
	
	/**
	 * Releases the OpenGL resources for the renderer. The OpenGL
	 * context must be active.
	 */
	void release();
	
	/**
	 * Sets the scale to draw at.
	 *
	 * @param scale  The scale to set.
	 */
	void setScale(float scale);
	
	/**
	 * Returns the current scale.
	 *
	 * @return The scale.
	 */
	float scale() const;

	/**
	 * Sets the X rotation in degrees.
	 *
	 * @param rot  The rotation to set.
	 */
	void setRotationX(float rot);
	
	/**
	 * Returns the X rotation in degrees.
	 *
	 * @return The rotation in degrees.
	 */
	float rotationX() const;
	
	/**
	 * Sets the Y rotation in degrees.
	 *
	 * @param rot  The rotation to set.
	 */
	void setRotationY(float rot);
	
	/**
	 * Returns the Y rotation in degrees.
	 *
	 * @return The rotation in degrees.
	 */
	float rotationY() const;
	
	/**
	 * Sets the current panning.
	 *
	 * @param The panning to set.
	 */
	void setPan(const glm::vec2 &pan);
	
	/**
	 * Returns the current panning.
	 *
	 * @return The current panning.
	 */
	glm::vec2 pan() const;
	
	/**
	 * Sets the contrast
	 *
	 * @param intensity  The contrast to set.
	 */
	void setContrast(float contrast);
	
	/**
	 * Returns the contrast.
	 *
	 * @return The contrast.
	 */
	float contrast() const;

	/**
	 * Calculates a start position and a direction vector for the specified point
	 * on the view field.
	 */
	void screenRay(const glm::vec2 &screenPos, glm::vec4 *start, glm::vec4 *direction) const;

	/**
	 * Calls the \c coneSelect method on the simulator with a ray based on the specified screen
	 * coordinates.
	 *
	 * @param pos     The screen position.
	 * @param radius  The cone radius.
	 * @param depth   The selection radius.
	 *
	 * @return Whether any selection was made.
	 */
	bool coneSelect(const glm::vec2 &pos, float radius, float selectionRadius);

	/**
	 * Calls the \c coneCut method on the simulator with a ray based on the specified screen
	 * coordinates.
	 *
	 * @param pos              The screen position.
	 * @param radius           The cone radius.
	 *
	 * @return The number of points affected.
	 */
	bool coneCut(const glm::vec2 &pos, float radius);
	
	/**
	 * Moves the current selection in the simulator to the current screen position.
	 *
	 * @param dest  The new position of the selection origin.
	 */
	void moveSelection(const glm::vec2 &dest);

	/**
	 * Sets whether to draw links or just points.
	 *
	 * @param enable  \c true to enable, \c false to disable
	 */
	void setDrawLinks(bool enable);

	/**
	 * Returns \c true if the renderer is set to draw links.
	 *
	 * @return Whether links are drawn or not.
	 */
	bool drawLinks() const;

	/**
	 * Rebuilds the link index buffer to reflect changes in links.
	 */
	void rebuildLinks();

	~Renderer();
	
private:
	Renderer(const Renderer &);
	Renderer &operator=(const Renderer &);

	void setupVertexArray();
	static bool compileShader(GLuint shader, const char *shaderSource);
	glm::mat4 modelViewMatrix() const;
	glm::mat4 projectionMatrix() const;
	glm::vec2 toNormalizedDevice(const glm::vec2 &pos) const;
	
	bool _initialized;
	Simulator *_simulator;
	
	GLuint _vertexArray;
	GLuint _pointBuffer;
	GLuint _linkIndexBuffer;
	int _nIndexes;
	GLuint _shaderProgram;
	GLuint _vertexShader;
	GLuint _fragmentShader;
	
	GLint _pjMatrixLocation;
	GLint _mvMatrixLocation;
	GLint _contrastLocation;
	GLint _fogLocation;
	
	float _fovY;
	float _width;
	float _height;
	float _scale;
	float _rotX;
	float _rotY;
	float _contrast;
	bool _drawLinks;
	glm::vec2 _pan;
	glm::mat4 _projectionMatrix;
};

#endif /* defined(__BioSim__Renderer__) */
