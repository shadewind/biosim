#ifndef BioSim_log_h
#define BioSim_log_h

#define LOG(...) doLog(__VA_ARGS__)

void doLog(const char *format, ...);

#endif
