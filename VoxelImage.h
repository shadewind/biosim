#ifndef BioSim_VoxelImage_h
#define BioSim_VoxelImage_h

#include <cstddef>

/**
 * Represents a voxel image with float voxel values.
 */
class VoxelImage {
public:
	/**
	 * Constructs a new voxel image with the specified parameters.
	 *
	 * @param width          The width.
	 * @param height         The height.
	 * @param depth	         The depth.
	 * @param numComponents  The number of components per voxel.
	 * @param data           A point to the data of the voxel image. If this is \c NULL
	 *                       the data will instead be allocated and zero filled.
	 */
	VoxelImage(int width, int height, int depth, int numComponents, float *data);
	
	/**
	 * Creates a copy of the specified voxel image.
	 *
	 * @param other  The image to copy.
	 */
	VoxelImage(const VoxelImage &other);

	/**
	 * Returns the width.
	 *
	 * @return The width.
	 */
	int width() const;
	
	/**
	 * Returns the height.
	 *
	 * @return The height.
	 */
	int height() const;
	
	/**
	 * Returns the depth.
	 *
	 * @return The depth.
	 */
	int depth() const;
	
	/**
	 * Returns the number of components per element.
	 *
	 * @return The number of components.
	 */
	int numComponents() const;
	
	/**
	 * Returns a pointer to the data of the frame.
	 *
	 * @return The data pointer.
	 */
	const float *data() const;
	
	/**
	 * Returns a const pointer to the specified voxel.
	 *
	 * @param x  The X coordinate.
	 * @param y  The Y coordinate.
	 * @param z  The Z coordinate.
	 */
	const float *voxel(int x, int y, int z) const;
	
	/**
	 * Returns a pointer to the specified voxel.
	 *
	 * @param x  The X coordinate.
	 * @param y  The Y coordinate.
	 * @param z  The Z coordinate.
	 */
	float *voxel(int x, int y, int z);
	
	/**
	 * Returns the total size of the data in bytes.
	 */
	size_t byteSize() const;
	
	/**
	 * Fills the image with the specified value.
	 *
	 * @param values  A pointer to an array containing the values for every component.
	 */
	void fill(float *values);
	
	/**
	 * Returns a downsampled version if this voxel image. The new image will be half
	 * the size of this one.
	 */
	VoxelImage *downsampled() const;
	
	/**
	 * Returns a downsampled version if this voxel image which is downsampled \c n times. The new image
	 * will be 1/n the size of the original.
	 */
	VoxelImage *downsampled(int n) const;
	
	~VoxelImage();

private:
	VoxelImage &operator=(const VoxelImage &);

	int _width;
	int _height;
	int _depth;
	int _numComponents;
	float *_data;
};

#endif
