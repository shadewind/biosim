#ifndef __BioSim__Simulator__
#define __BioSim__Simulator__

#include "opencl.h"
#include "opengl.h"

#include <glm/glm.hpp>

#include "VoxelImage.h"
#include "PointData.h"

/// Special value used to specify that a link is unused and should not be processed.
#define NO_POINT	(4294967295U)

class Renderer;

class Simulator {	
public:
	Simulator();
	
	/**
	 * Initializes the \c Simulator.
	 *
	 * @param context   The OpenCL context. This must have OpenGL sharing with the active OpenGL context enabled.
	 * @param image     The point data to use for simulation.
	 *
	 * @return \c true on success, \c false on failure.
	 */
	bool initialize(cl_context context, const PointData *pointData);
	
	/**
	 * Returns the number of points in the simulation.
	 *
	 * @return The number of points.
	 */
	int nPoints() const;
	
	/**
	 * Advances the simulation by the specified amount of time.
	 *
	 * @param time  The time in seconds.
	 */
	void advanceSimulation(float time);

	/**
	 * Increases time by the timestep and performs a step immediately.
	 */
	void step();
	
	/**
	 * Sets the timestep. Changing while the simulation is running _will_ result in
	 * weird results.
	 *
	 * @param timestep  The timestep in seconds.
	 */
	void setTimestep(float timestep);

	/**
	 * Returns the timestep.
	 *
	 * @return The timestep in seconds.
	 */
	float timestep() const;
	
	/**
	 * Returns the point buffer object.
	 *
	 * @return The OpenCL point buffer object.
	 */
	cl_mem pointBuffer();

	/**
	 * Returns the stride of the point buffer.
	 *
	 * @return The point buffer stride.
	 */
	size_t pointBufferStride() const;
	
	/**
	 * Releases all the resources used by the simulator. Since this will also release
	 * GL resources created by the simulator, the OpenGL context must also be active.
	 */
	void release();

	/**
	 * Chooses the closest point which falls inside an infinite cone described by the specified
	 * a ray with an origin and direction as well as a radius which describes the radius of
	 * the cone at a distance of 1.0 from its tip. Points will selected recursively through links
	 * from the chosen point.
	 *
	 * If there is a cone hit, the previous selection will be cleared.
	 *
	 * @param start            The ray start.
	 * @param dir              The ray direction.
	 * @param radius           The cone radius 1.0 distance.
	 * @param selectionRadius  The selection radius.
	 *
	 * @return The number of selected points.
	 */
	bool coneSelect(const glm::vec4 &start, const glm::vec4 &dir, float radius, float selectionRadius);

	/**
	 * Transforms all selected points.
	 *
	 * @param transform  The transform matrix to apply.
	 */
	void transformSelection(const glm::mat4 &transform);

	/**
	 * Returns the origin of the last selection.
	 *
	 * @return The selection origin.
	 */
	glm::vec4 selectionOrigin() const;

	/**
	 * Returns true if there any points are currently selected.
	 *
	 * @return Whether any points are selected or not.
	 */
	bool hasSelection() const;

	/**	
	 * Removes all links for all points that fall within a cone described by a starting point,
	 * direction and radius at a distance of 1.0. The "other side" of each link will also be removed.
	 *
	 * @param start   The start of the cone.
	 * @param dir     The direction of the cone.
	 * @param radius  The radius of the cone at a distance of 1.0.
	 *
	 * @return The number of affected points.
	 */
	bool coneCut(const glm::vec4 &start, const glm::vec4 &dir, float radius);

	/**
	 * Returns the current simulation time.
	 *
	 * @return The simulation time in seconds.
	 */
	float time() const;

	/**
	 * Returns the average calculation time of each timestep. This is a running average.
	 *
	 * @return The average timestep duration.
	 */
	float avgTimeStep() const;

	/**
	 * Checks whether simulation has exploded or not.
	 *
	 * @return \c true if the simulation has exploded, \c false if not.
	 */
	bool hasExploded();

	/**
	 * Sets the spring scale to use for the simulation. Must be set before initialization.
	 *
	 * @param scale  The spring scale to set.
	 */
	void setSpringScale(float scale);

	/**
	 * Sets the damper scale to use for the simulation. Must be set before initialization.
	 *
	 * @param scale  The damper scale to set.
	 */
	void setDamperScale(float scale);

	/**
	 * Returns the total number of steps that have been taken.
	 *
	 * @return  The total number of steps.
	 */
	int nSteps() const;

	/**
	 * Reads the point data from OpenCL and returns it as a \c PointData object.
	 * This object must be released by the caller.
	 *
	 * @return The point data object or \c NULL on failure.
	 */
	PointData *readPointData();

	/**
	 * If set to \c true, a fixed constant force will be applied
	 * as defined in the kernel source.
	 *
	 * @param enable  \c true to enable, \c false to disable.
	 */
	void setTestForce(bool enable);
	
	/**
	 * Deletes this object and releases all of the resources used automatically.
	 */
	~Simulator();
	
private:
	Simulator(const Simulator &);
	Simulator &operator=(const Simulator &);

	bool createKernels();
	cl_kernel createKernel(const char *name);
	void doAdvanceSimulation(float dt);
	bool loadData(const PointData *pointData);
	bool allocateBuffers();
	void releaseBuffers();
	void printDeviceInfo();
	bool acquirePointBuffer();
	bool releasePointBuffer();
	PointData *readPoints();
	bool writePoints(const PointData *data);
	void calculateNDRange(size_t actualItems, size_t *globalItems, size_t *workGroupSize);

	float _springScale;
	float _damperScale;
	bool _testForce;
	bool _initialized;

	cl_context _context;
	cl_device_id _device;
	cl_command_queue _commandQueue;

	int _nPoints;
	size_t _stride;
	size_t _nLinks;
	size_t _padding;

	GLuint _pointBufferGl;
	cl_mem _pointBuffer;
	cl_mem _dbuffer1;
	cl_mem _dbuffer2;
	cl_mem _dbuffer3;
	cl_mem _dbuffer4;

	cl_program _program;
	cl_kernel _evaluateKernel;
	cl_kernel _integrateKernel;
	cl_kernel _transformSelectionKernel;
	cl_kernel _clearConeLinksKernel;

	bool _hasSelection;
	glm::vec4 _selectionOrigin;
	
	float _timestep;
	float _accTime;
	float _simulationTime;
	int _nSteps;
	float _avgStepTime;
};

#endif /* defined(__BioSim__Simulator__) */
