#include "Renderer.h"

#include <cmath>
#include <vector>
#include <memory>

#include "opencl.h"

#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "log.h"
#include "utils.h"
#include "Simulator.h"

Renderer::Renderer() :
	_vertexArray(0), _pointBuffer(0), _shaderProgram(0), _vertexShader(0), _fragmentShader(0),
	_scale(1.0f / 40.0f), _initialized(false), _rotX(25.0f), _rotY(45.0f), _contrast(1.0f),
	_simulator(NULL), _fovY(70.0f), _width(1.0f), _height(1.0f), _drawLinks(false)
{
}

bool Renderer::initialize(Simulator *simulator)
{
	_simulator = simulator;
	_shaderProgram = glCreateProgram();
	_vertexShader = glCreateShader(GL_VERTEX_SHADER);
	if (!compileShader(_vertexShader, "shaders/vertex.vs"))
		return false;
	_fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	if (!compileShader(_fragmentShader, "shaders/fragment.fs"))
		return false;
	
	glAttachShader(_shaderProgram, _vertexShader);
	glAttachShader(_shaderProgram, _fragmentShader);
	glLinkProgram(_shaderProgram);
	GLint status;
	glGetProgramiv(_shaderProgram, GL_LINK_STATUS, &status);
	if (status == GL_TRUE) {
		LOG("INFO: Linking of shader program successful.");
	} else {
		GLint logLength;
		glGetProgramiv(_shaderProgram, GL_INFO_LOG_LENGTH, &logLength);
		char *log = new char[logLength];
		glGetProgramInfoLog(_shaderProgram, logLength, NULL, log);
		LOG("ERROR: Linking of shader program failed:\n%s", log);
		delete log;
		return false;
	}
	
	glGenVertexArrays(1, &_vertexArray);
	glGenBuffers(1, &_linkIndexBuffer);
	glBindVertexArray(_vertexArray);
	clGetGLObjectInfo(simulator->pointBuffer(), NULL, &_pointBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, _pointBuffer);

	GLint positionLocation = glGetAttribLocation(_shaderProgram, "vertexPosition");
	glVertexAttribPointer(positionLocation, 4, GL_FLOAT, GL_FALSE, _simulator->pointBufferStride(), (GLvoid *)0);
	glEnableVertexAttribArray(positionLocation);
	
	GLint intensityLocation = glGetAttribLocation(_shaderProgram, "vertexIntensity");
	glVertexAttribPointer(intensityLocation, 1, GL_FLOAT, GL_FALSE, _simulator->pointBufferStride(), (GLvoid *)(sizeof(float) * 8));
	glEnableVertexAttribArray(intensityLocation);

	GLint selectanceLocation = glGetAttribLocation(_shaderProgram, "vertexSelectance");
	glVertexAttribPointer(selectanceLocation, 1, GL_FLOAT, GL_FALSE, _simulator->pointBufferStride(), (GLvoid *)(sizeof(float) * 9));
	glEnableVertexAttribArray(selectanceLocation);
	
	_pjMatrixLocation = glGetUniformLocation(_shaderProgram, "projectionMatrix");
	_mvMatrixLocation = glGetUniformLocation(_shaderProgram, "modelviewMatrix");
	_contrastLocation = glGetUniformLocation(_shaderProgram, "contrast");
	_fogLocation = glGetUniformLocation(_shaderProgram, "fog");

	rebuildLinks();
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);
	glFinish();
	
	_initialized = true;
	return true;
}

void Renderer::rebuildLinks()
{
	std::vector<uint32_t> indexes;

	PointData *data = _simulator->readPointData();
	int nPoints = data->nPoints();
	int nLinks = data->nLinks();
	for (int i = 0; i < nPoints; i++) {
		PointData::Link *links = data->linksAt(i);
		for (int l = 0; l < nLinks; l++) {
			if (links[l].partnerIndex == NO_POINT)
				continue;
			
			indexes.push_back(i);
			indexes.push_back(links[l].partnerIndex);
			//Remove the link in the other direction, we don't want duplicates.
			data->removeLinks(links[l].partnerIndex, i);
		}
	}
	delete data;
	_nIndexes = indexes.size();

	glBindVertexArray(_vertexArray);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _linkIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, _nIndexes * sizeof(uint32_t), indexes.data(), GL_STATIC_DRAW);
}

void Renderer::release()
{
	if (!_initialized)
		return;

	glDeleteVertexArrays(1, &_vertexArray);
	glDeleteBuffers(1, &_linkIndexBuffer);
	glDeleteProgram(_shaderProgram);
	glDeleteShader(_vertexShader);
	glDeleteShader(_fragmentShader);
}

void Renderer::draw()
{
	if (_simulator == NULL)
		return;
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(_shaderProgram);
	glUniformMatrix4fv(_pjMatrixLocation, 1, GL_FALSE, glm::value_ptr(projectionMatrix()));
	glUniformMatrix4fv(_mvMatrixLocation, 1, GL_FALSE, glm::value_ptr(modelViewMatrix()));
	glUniform1f(_contrastLocation, _contrast);
	glUniform1i(_fogLocation, _drawLinks);
	
	glBindVertexArray(_vertexArray);
	if (_drawLinks) {
		glDrawElements(GL_LINES, _nIndexes, GL_UNSIGNED_INT, 0);
	} else {
		glPointSize(2.0f);
		glDrawArrays(GL_POINTS, 0, _simulator->nPoints());
	}
}

void Renderer::reshape(int width, int height)
{
	glViewport(0, 0, width, height);
	_width = width;
	_height = height;
}

bool Renderer::compileShader(GLuint shader, const char *shaderSourceFile)
{
	GLchar *source;
	size_t length;
	if (!loadResource(shaderSourceFile, &source, &length)) {
		LOG("ERROR: Unable to open shader source file '%s'.", shaderSourceFile);
		return false;
	}
	
	GLsizei gllength = (GLsizei)length;
	glShaderSource(shader, 1, (const GLchar **)&source, &gllength);
	glCompileShader(shader);
	delete[] source;
	
	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (status != GL_TRUE) {
		GLint logLength;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
		char *log = new char[logLength];
		glGetShaderInfoLog(shader, logLength, NULL, log);
		LOG("ERROR: Compilation of shader '%s' failed:\n%s", shaderSourceFile, log);
		delete[] log;
		return false;
	}

	return true;
}

void Renderer::setScale(float scale)
{
	_scale = scale;
}

float Renderer::scale() const
{
	return _scale;
}

void Renderer::setRotationX(float rot)
{
	_rotX = fmodf(rot, 360.0f);
}

float Renderer::rotationX() const
{
	return _rotX;
}

void Renderer::setRotationY(float rot)
{
	_rotY = fmodf(rot, 360.0f);
}

float Renderer::rotationY() const
{
	return _rotY;
}

void Renderer::setPan(const glm::vec2 &pan)
{
	_pan = pan;
}

glm::vec2 Renderer::pan() const
{
	return _pan;
}

void Renderer::setContrast(float contrast)
{
	_contrast = contrast;
}

float Renderer::contrast() const
{
	return _contrast;
}

void Renderer::setDrawLinks(bool enable)
{
	_drawLinks = enable;
}

bool Renderer::drawLinks() const
{
	return _drawLinks;
}

glm::mat4 Renderer::modelViewMatrix() const
{
	return 
		glm::translate(_pan.x, _pan.y, -1.0f) *
		glm::rotate(_rotX, 1.0f, 0.0f, 0.0f) *
		glm::rotate(_rotY, 0.0f, 1.0f, 0.0f) *
		glm::scale(_scale, _scale, _scale);
}

glm::mat4 Renderer::projectionMatrix() const
{
	return glm::perspective(_fovY, _width / _height, 0.01f, 100.0f);
}

glm::vec2 Renderer::toNormalizedDevice(const glm::vec2 &pos) const
{
	return glm::vec2((pos.x / (_width / 2.0f)) - 1.0f, (pos.y / (_height / 2.0f)) - 1.0f);
}

void Renderer::screenRay(const glm::vec2 &screenPos, glm::vec4 *start, glm::vec4 *direction) const
{
	glm::mat4 projection = projectionMatrix();
	glm::mat4 modelView = modelViewMatrix();

	// Projecting a direction vector (a vector with w = 0) results in
	// (x / -z, y / -z, A, 1.0). By taking the inverse of the projection matrix
	// and assuming a Z of -1.0 (doesn't matter since we later normalize but -1.0
	// cancels out nicely) we get the direction vector just like that. A can be found
	// as -A in the projection matrix at [2][2].
	glm::vec4 p(toNormalizedDevice(screenPos), -projection[2][2], 1.0f);
	*start = glm::inverse(modelView)[3];
	*direction = glm::normalize(glm::inverse(projection * modelView) * p);
}

bool Renderer::coneSelect(const glm::vec2 &pos, float radius, float selectionRadius)
{
	glm::vec4 start, dir;
	screenRay(pos, &start, &dir);
	return _simulator->coneSelect(start, dir, radius, selectionRadius);
}

bool Renderer::coneCut(const glm::vec2 &pos, float radius)
{
	glm::vec4 start, dir;
	screenRay(pos, &start, &dir);
	return _simulator->coneCut(start, dir, radius);
}

void Renderer::moveSelection(const glm::vec2 &dest)
{
	if (!_simulator->hasSelection())
		return;

	glm::vec4 rayStart, rayDir;
	screenRay(dest, &rayStart, &rayDir);
	glm::vec4 selectionOrigin = _simulator->selectionOrigin();
	float distance = glm::distance(rayStart, selectionOrigin);
	glm::vec4 worldDest = rayStart + (rayDir * distance);
	glm::vec4 delta = worldDest - selectionOrigin;
	glm::mat4 transform = glm::translate(delta.x, delta.y, delta.z);
	_simulator->transformSelection(transform);
}

Renderer::~Renderer()
{
	release();
}
