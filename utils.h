#ifndef BioSim_utils_h
#define BioSim_utils_h

#include <cstddef>

#define GL_CHECK_ERROR() \
	do { \
		GLenum error = glGetError(); \
		if(error != GL_NO_ERROR) \
			LOG("GL error (%s:%d): %s\n", __FILE__, __LINE__, gluErrorString(error)); \
	} while(0)

/**
 * Finds the specified resource and loads the contents into a memory buffer.
 *
 * @param name      The name of the resource.
 * @param outBuf    Will be set to a pointer to a newly created buffer. The caller is responsible for deleting this.
 * @param outLen    Will be set to the length of the data.
 *
 * @return \c true on success, \c false on failure.
 */
bool loadResource(const char *name, char **outBuf, size_t *outLen);

/**
 * Loads the contents of the specified file into a memory buffer.
 *
 * @param filename  The name of the file.
 * @param outBuf    Will be set to a pointer to a newly created buffer. The caller is responsible for deleting this.
 * @param outLen    Will be set to the length of the data.
 *
 * @return \c true on success, \c false on failure.
 */
bool loadContentsOfFile(const char *filename, char **outBuf, size_t *outLen);

/**
 * Returns the current system time in seconds relative to nothing in particular.
 */
double getSystemTime();

#endif
