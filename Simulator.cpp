
#include "Simulator.h"

#include <stdint.h>

#include <glm/gtc/type_ptr.hpp>

#include "log.h"
#include "Renderer.h"
#include "PointData.h"
#include "utils.h"

// Threshold velocity for detecting explosions.
#define EXPLOSION_THRESHOLD 100000.0f

Simulator::Simulator() :
	_initialized(false), _context(NULL), _device(NULL), _commandQueue(NULL), _pointBuffer(NULL), _pointBufferGl(0), _dbuffer1(NULL),
	_dbuffer2(NULL), _dbuffer3(NULL), _dbuffer4(NULL), _program(NULL), _evaluateKernel(NULL), _integrateKernel(NULL), _timestep(0.1f),
	_accTime(0.0f), _simulationTime(0.0f), _nSteps(0), _avgStepTime(0.0f), _transformSelectionKernel(NULL), _springScale(1000.0f),
	_damperScale(15.0f), _hasSelection(false), _clearConeLinksKernel(NULL), _testForce(false)
{
}

bool Simulator::initialize(cl_context context, const PointData *pointData)
{
	cl_int error;

	if (_initialized)
		return false;

	_context = context;
	
	//Find the device.
	cl_uint numDevices;
	clGetContextInfo(_context, CL_CONTEXT_NUM_DEVICES, sizeof(numDevices), &numDevices, NULL);
	cl_device_id *devices = new cl_device_id[numDevices];
	clGetContextInfo(_context, CL_CONTEXT_DEVICES, sizeof(cl_device_id) * numDevices, devices, NULL);
	_device = devices[0];
	delete[] devices;

	printDeviceInfo();
		
	_commandQueue = clCreateCommandQueue(_context, _device, 0, &error);
	if (error != CL_SUCCESS) {
		LOG("ERROR: Unable to create command queue.");
		release();
		return false;
	}
	
	if (!loadData(pointData) || !createKernels()) {
		release();
		return false;
	}

	clFinish(_commandQueue);
	_initialized = true;
	return true;
}

void Simulator::printDeviceInfo()
{
	char deviceName[256];
	clGetDeviceInfo(_device, CL_DEVICE_NAME, sizeof(deviceName), deviceName, NULL);
	LOG("INFO: Device name: %s", deviceName);

	cl_ulong globalMem;
	clGetDeviceInfo(_device, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(globalMem), &globalMem, NULL);
	cl_ulong localMem;
	clGetDeviceInfo(_device, CL_DEVICE_LOCAL_MEM_SIZE, sizeof(localMem), &localMem, NULL);
	LOG("INFO: Device memory: %d MB (global) / %d kB (local)", globalMem / (1024 * 1024), localMem / 1024);

	char deviceVersion[256];
	clGetDeviceInfo(_device, CL_DEVICE_VERSION, sizeof(deviceVersion), deviceVersion, NULL);
	LOG("INFO: Device version: %s", deviceVersion);
	char deviceCVersion[256];
	clGetDeviceInfo(_device, CL_DEVICE_OPENCL_C_VERSION, sizeof(deviceCVersion), deviceCVersion, NULL);
	LOG("INFO: Device OpenCL C version: %s", deviceCVersion);
	char driverVersion[256];
	clGetDeviceInfo(_device, CL_DRIVER_VERSION, sizeof(driverVersion), driverVersion, NULL);
	LOG("INFO: Device driver version: %s", driverVersion);
}

bool Simulator::createKernels()
{
	cl_int error;
	char *programSource;
	size_t sourceLength;
	if (!loadResource("kernels/simulate.cl", &programSource, &sourceLength)) {
		LOG("ERROR: Unable to load 'simulate.cl'.");
		return false;
	}
	_program = clCreateProgramWithSource(_context, 1, (const char **)&programSource, &sourceLength, &error);
	delete[] programSource;
	if (error != CL_SUCCESS) {
		LOG("ERROR: Unable to create program object from source.");
		return false;
	}
	
	char compileArgs[512];
	const char *testForceString = _testForce ? "-D TEST_FORCE" : "";
	snprintf(compileArgs, sizeof(compileArgs),
		"-D N_LINKS=%d -D PADDING=%d -D SPRING_SCALE=%ff -D DAMPER_SCALE=%ff %s -cl-std=CL1.1",
		(int)_nLinks, (int)_padding, _springScale, _damperScale, testForceString);
	if (clBuildProgram(_program, 1, &_device, compileArgs, NULL, NULL) != CL_SUCCESS) {
		LOG("ERROR: Unable to build OpenCL program.");
		size_t logLength;
		//Determine size of log
		clGetProgramBuildInfo(_program, _device, CL_PROGRAM_BUILD_LOG, 0, NULL, &logLength);
		char *log = new char[logLength];
		clGetProgramBuildInfo(_program, _device, CL_PROGRAM_BUILD_LOG, logLength, log, NULL);
		LOG("%s", log);
		delete[] log;
		
		return false;
	}
	
	_evaluateKernel = createKernel("evaluate");
	_integrateKernel = createKernel("integrate");
	_transformSelectionKernel = createKernel("transformSelection");
	_clearConeLinksKernel = createKernel("clearConeLinks");

	return
		_evaluateKernel &&
		_integrateKernel &&
		_transformSelectionKernel &&
		_clearConeLinksKernel;
}

cl_kernel Simulator::createKernel(const char *name)
{
	cl_int error;
	cl_kernel kernel = clCreateKernel(_program, name, &error);
	if (error != CL_SUCCESS)
		LOG("ERROR: Unable to create '%s' kernel.", name);

	return kernel;
}

bool Simulator::loadData(const PointData *pointData)
{
	_stride = pointData->stride();
	_nLinks = pointData->nLinks();
	_padding = pointData->padding();
	_nPoints = pointData->nPoints();

	if (!allocateBuffers())
		return false;
	
	if (acquirePointBuffer()) {
		writePoints(pointData);
		releasePointBuffer();
	}
	clFinish(_commandQueue);

	return true;
}

bool Simulator::allocateBuffers()
{
	cl_int error;
	
	glGenBuffers(1, &_pointBufferGl);
	glBindBuffer(GL_ARRAY_BUFFER, _pointBufferGl);
	glBufferData(GL_ARRAY_BUFFER, _stride * _nPoints, NULL, GL_DYNAMIC_COPY);
	glFinish();
	_pointBuffer = clCreateFromGLBuffer(_context, CL_MEM_READ_WRITE, _pointBufferGl, &error);
	if (error != CL_SUCCESS) {
		LOG("ERROR: Unable to create point buffer from OpenGL buffer.");
		return false;
	}
	
	_dbuffer1 = clCreateBuffer(
		_context, CL_MEM_READ_WRITE,
		sizeof(PointData::PhysicsState) * _nPoints, NULL, &error);
	if (error != CL_SUCCESS) {
		LOG("ERROR: Unable to allocate dbuffer1.");
		return false;
	}
	
	_dbuffer2 = clCreateBuffer(
		_context, CL_MEM_READ_WRITE,
		sizeof(PointData::PhysicsState) * _nPoints, NULL, &error);
	if (error != CL_SUCCESS) {
		LOG("ERROR: Unable to allocate dbuffer2.");
		return false;
	}
	
	_dbuffer3 = clCreateBuffer(
		_context, CL_MEM_READ_WRITE,
		sizeof(PointData::PhysicsState) * _nPoints, NULL, &error);
	if (error != CL_SUCCESS) {
		LOG("ERROR: Unable to allocate dbuffer3.");
		return false;
	}
	
	_dbuffer4 = clCreateBuffer(
		_context, CL_MEM_READ_WRITE,
		sizeof(PointData::PhysicsState) * _nPoints, NULL, &error);
	if (error != CL_SUCCESS) {
		LOG("ERROR: Unable to allocate dbuffer4.");
		return false;
	}
	
	return true;
}

int Simulator::nPoints() const
{
	return _nPoints;
}

cl_mem Simulator::pointBuffer()
{
	return _pointBuffer;
}

size_t Simulator::pointBufferStride() const
{
	return _stride;
}

void Simulator::advanceSimulation(float time)
{
	if (!_initialized)
		return;
	
	_accTime += time;
	while (_accTime > _timestep) {
		doAdvanceSimulation(_timestep);
		_accTime -= _timestep;
	}
}

void Simulator::step()
{
	if (!_initialized)
		return;

	doAdvanceSimulation(_timestep);
}

void Simulator::doAdvanceSimulation(float dt)
{
	cl_int error;
	double startTime = getSystemTime();

	//LOG("Advancing...");
	size_t globalItems, workGroupSize;
	calculateNDRange(_nPoints, &globalItems, &workGroupSize);
	float dtHalf = dt / 2.0f;
	
	if (!acquirePointBuffer())
		return;
	
	clSetKernelArg(_evaluateKernel, 0, sizeof(cl_mem), &_pointBuffer);
	clSetKernelArg(_evaluateKernel, 3, sizeof(cl_int), &_nPoints);
	clSetKernelArg(_evaluateKernel, 5, sizeof(cl_float), &_simulationTime);
	
	//Calculate d1
	clSetKernelArg(_evaluateKernel, 1, sizeof(cl_mem), NULL);
	clSetKernelArg(_evaluateKernel, 2, sizeof(cl_mem), &_dbuffer1);
	clSetKernelArg(_evaluateKernel, 4, sizeof(cl_float), &dt);
	error = clEnqueueNDRangeKernel(_commandQueue, _evaluateKernel, 1, NULL, &globalItems, &workGroupSize, 0, NULL, NULL);
	if (error != CL_SUCCESS)
		LOG("ERROR: Failed to enqueue calculation of d1 using evaluate kernel.");
	
	//Calculate d2
	clSetKernelArg(_evaluateKernel, 1, sizeof(cl_mem), &_dbuffer1);
	clSetKernelArg(_evaluateKernel, 2, sizeof(cl_mem), &_dbuffer2);
	clSetKernelArg(_evaluateKernel, 4, sizeof(cl_float), &dtHalf);
	error = error = clEnqueueNDRangeKernel(_commandQueue, _evaluateKernel, 1, NULL, &globalItems, &workGroupSize, 0, NULL, NULL);
	if (error != CL_SUCCESS)
		LOG("ERROR: Failed to enqueue calculation of d2 using evaluate kernel.");
	
	//Calculate d3
	clSetKernelArg(_evaluateKernel, 1, sizeof(cl_mem), &_dbuffer2);
	clSetKernelArg(_evaluateKernel, 2, sizeof(cl_mem), &_dbuffer3);
	clSetKernelArg(_evaluateKernel, 4, sizeof(cl_float), &dtHalf);
	error = clEnqueueNDRangeKernel(_commandQueue, _evaluateKernel, 1, NULL, &globalItems, &workGroupSize, 0, NULL, NULL);
	if (error != CL_SUCCESS)
		LOG("ERROR: Failed to enqueue calculation of d3 using evaluate kernel.");
	
	//Calculate d4
	clSetKernelArg(_evaluateKernel, 1, sizeof(cl_mem), &_dbuffer3);
	clSetKernelArg(_evaluateKernel, 2, sizeof(cl_mem), &_dbuffer4);
	clSetKernelArg(_evaluateKernel, 4, sizeof(cl_float), &dt);
	error = clEnqueueNDRangeKernel(_commandQueue, _evaluateKernel, 1, NULL, &globalItems, &workGroupSize, 0, NULL, NULL);
	if (error != CL_SUCCESS)
		LOG("ERROR: Failed to enqueue calculation of d4 using evaluate kernel.");
	
	clSetKernelArg(_integrateKernel, 0, sizeof(cl_mem), &_pointBuffer);
	clSetKernelArg(_integrateKernel, 1, sizeof(cl_mem), &_dbuffer1);
	clSetKernelArg(_integrateKernel, 2, sizeof(cl_mem), &_dbuffer2);
	clSetKernelArg(_integrateKernel, 3, sizeof(cl_mem), &_dbuffer3);
	clSetKernelArg(_integrateKernel, 4, sizeof(cl_mem), &_dbuffer4);
	clSetKernelArg(_integrateKernel, 5, sizeof(cl_int), &_nPoints);
	clSetKernelArg(_integrateKernel, 6, sizeof(cl_float), &dt);
	error = clEnqueueNDRangeKernel(_commandQueue, _integrateKernel, 1, NULL, &globalItems, &workGroupSize, 0, NULL, NULL);
	if (error != CL_SUCCESS)
		LOG("ERROR: Failed to enqueue integrate kernel.");

	releasePointBuffer();
	clFinish(_commandQueue);
	_simulationTime += dt;

	double endTime = getSystemTime();
	double delta = endTime - startTime;
	_avgStepTime = (delta + (_avgStepTime * _nSteps)) / (_nSteps + 1);

	_nSteps++;
}

bool Simulator::acquirePointBuffer()
{
	//glFinish to make sure OpenGL is done with the objects.
	glFinish();
	cl_int error = clEnqueueAcquireGLObjects(_commandQueue, 1, &_pointBuffer, 0, NULL, NULL);
	if (error != CL_SUCCESS) {
		LOG("ERROR: Unable to enqueue acquisition of OpenGL objects.");
		return false;
	}

	return true;
}

bool Simulator::releasePointBuffer()
{
	cl_int error = clEnqueueReleaseGLObjects(_commandQueue, 1, &_pointBuffer, 0, NULL, NULL);
	if (error != CL_SUCCESS) {
		LOG("ERROR: Failed to enqueue release of OpenGL objects.");
		return false;
	}

	return true;
}

PointData *Simulator::readPoints()
{
	size_t size = _stride * _nPoints;
	uint8_t *pointData = new uint8_t[size];
	cl_int error = clEnqueueReadBuffer(_commandQueue, _pointBuffer, CL_TRUE, 0, size, pointData, 0, NULL, NULL);

	if (error != CL_SUCCESS) {
		LOG("ERROR: Unable to read points from device memory.");
		return NULL;
	}

	return new PointData(pointData, _nLinks, _nPoints);
}

bool Simulator::writePoints(const PointData *data) {
	cl_int error = clEnqueueWriteBuffer(
		_commandQueue, _pointBuffer,
		CL_TRUE, 0, data->dataSize(),
		data->pointData(), 0, NULL, NULL);

	if (error != CL_SUCCESS) {
		LOG("ERROR: Failed to write points to device memory.");
		return 0;
	}

	return true;
}

bool Simulator::coneSelect(const glm::vec4 &start, const glm::vec4 &dir, float radius, float selectionRadius)
{
	if (!_initialized)
		return 0;

	glm::vec4 direction = glm::normalize(dir);
	int point = -1;
	float minDistance = INFINITY;	
	
	if (!acquirePointBuffer())
		return 0;

	PointData *pointData = readPoints();
	if (pointData == NULL)
		goto cleanup;
	
	for (int i = 0; i < _nPoints; i++) {
		glm::vec4 position = pointData->pointAt(i)->state.position;
		float distance = glm::dot((position - start), direction);
		glm::vec4 projection = direction * distance;
		float rayDistance = glm::distance(projection, (position - start));
		if ((rayDistance < (radius * distance)) && (distance < minDistance)) {
			point = i;
			minDistance = distance;
		}
	}

	pointData->clearSelection();
	if (point != -1) {
		_selectionOrigin = pointData->pointAt(point)->state.position;
		pointData->recursiveSelect(start + (direction * minDistance), selectionRadius, point);
	}

	writePoints(pointData);

cleanup:
	delete pointData;
	releasePointBuffer();
	_hasSelection = (point != -1);
	clFinish(_commandQueue);
	return _hasSelection;
}

bool Simulator::hasSelection() const
{
	return _hasSelection;
}

void Simulator::transformSelection(const glm::mat4 &transform)
{
	if (!_initialized)
		return;

	if (!acquirePointBuffer())
		return;

	clSetKernelArg(_transformSelectionKernel, 0, sizeof(cl_mem), &_pointBuffer);
	clSetKernelArg(_transformSelectionKernel, 1, sizeof(cl_int), &_nPoints);
	clSetKernelArg(_transformSelectionKernel, 2, sizeof(cl_float4), glm::value_ptr(transform[0]));
	clSetKernelArg(_transformSelectionKernel, 3, sizeof(cl_float4), glm::value_ptr(transform[1]));
	clSetKernelArg(_transformSelectionKernel, 4, sizeof(cl_float4), glm::value_ptr(transform[2]));
	clSetKernelArg(_transformSelectionKernel, 5, sizeof(cl_float4), glm::value_ptr(transform[3]));

	size_t globalItems, workGroupSize;
	calculateNDRange(_nPoints, &globalItems, &workGroupSize);
	cl_int error = clEnqueueNDRangeKernel(
		_commandQueue, _transformSelectionKernel,
		1, NULL, &globalItems, &workGroupSize, 0, NULL, NULL);
	if (error != CL_SUCCESS)
		LOG("ERROR: Failed to enqueue transform selection kernel.");
	else
		_selectionOrigin = transform * _selectionOrigin;

	releasePointBuffer();
	clFinish(_commandQueue);
}

bool Simulator::coneCut(const glm::vec4 &start, const glm::vec4 &dir, float radius)
{
	bool success = false;
 	glm::vec4 direction = glm::normalize(dir);
	
 	if (!acquirePointBuffer())
 		return 0;

 	clSetKernelArg(_clearConeLinksKernel, 0, sizeof(cl_mem), &_pointBuffer);
 	clSetKernelArg(_clearConeLinksKernel, 1, sizeof(cl_int), &_nPoints);
 	clSetKernelArg(_clearConeLinksKernel, 2, sizeof(cl_float4), glm::value_ptr(start));
 	clSetKernelArg(_clearConeLinksKernel, 3, sizeof(cl_float4), glm::value_ptr(direction));
 	clSetKernelArg(_clearConeLinksKernel, 4, sizeof(cl_float), &radius);

 	size_t globalItems, workGroupSize;
 	calculateNDRange(_nPoints, &globalItems, &workGroupSize);
 	cl_int error = clEnqueueNDRangeKernel(
 		_commandQueue, _clearConeLinksKernel,
 		1, NULL, &globalItems, &workGroupSize, 0, NULL, NULL);
 	if (error != CL_SUCCESS)
 		LOG("ERROR: Failed to enqueue clear cone links kernel.");
 	else
 		success = true;

 	releasePointBuffer();
 	clFinish(_commandQueue);
	return success;
}

void Simulator::calculateNDRange(size_t actualItems, size_t *globalItems, size_t *workGroupSize)
{
	size_t groupSize = 256;
	size_t items = actualItems;
	if ((items % groupSize) != 0)
		items = ((items / groupSize) + 1) * groupSize;

	*globalItems = items;
	*workGroupSize = groupSize;
}

bool Simulator::hasExploded()
{
	if (!_initialized)
		return false;

	bool exploded = false;
	if (!acquirePointBuffer())
		return false;

	PointData *pointData = readPoints();
	if (pointData == NULL)
		goto cleanup;
	
	for (int i = 0; i < _nPoints; i++) {
		glm::vec4 v = pointData->pointAt(i)->state.velocity;
		// Use an aribitrary threshold for detecting explosions. To be absoluely sure, we should probably
		// test for NaN instead but that will detect the expliosion much later which is a pain when
		// doing automated stability testing since it will take a lot more time!
		if ((v.x + v.y + v.z + v.w) > EXPLOSION_THRESHOLD) {
			exploded = true;
			break;
		}
	}

cleanup:
	delete pointData;
	releasePointBuffer();
	clFinish(_commandQueue);
	return exploded;
}

PointData *Simulator::readPointData()
{
	PointData *data = NULL;
	if (acquirePointBuffer()) {
		data = readPoints();
		releasePointBuffer();
		clFinish(_commandQueue);
	}

	return data;
}

glm::vec4 Simulator::selectionOrigin() const
{
	return _selectionOrigin;
}

void Simulator::setTimestep(float timestep)
{
	_timestep = timestep;
}

float Simulator::timestep() const
{
	return _timestep;
}

float Simulator::avgTimeStep() const
{
	return _avgStepTime;
}

void Simulator::setSpringScale(float scale)
{
	_springScale = scale;
}

void Simulator::setDamperScale(float scale)
{
	_damperScale = scale;
}

float Simulator::time() const
{
	return _simulationTime;
}

int Simulator::nSteps() const
{
	return _nSteps;
}

void Simulator::setTestForce(bool enable)
{
	_testForce = enable;
}

void Simulator::release()
{
	if (!_initialized)
		return;

	clReleaseProgram(_program);
	_program = NULL;
	clReleaseKernel(_evaluateKernel);
	_evaluateKernel = NULL;
	clReleaseKernel(_integrateKernel);
	_integrateKernel = NULL;
	clReleaseKernel(_transformSelectionKernel);
	_transformSelectionKernel = NULL;
	
	clReleaseMemObject(_pointBuffer);
	_pointBuffer = NULL;
	glDeleteBuffers(1, &_pointBufferGl);
	_pointBufferGl = 0;
	clReleaseMemObject(_dbuffer1);
	_dbuffer1 = NULL;
	clReleaseMemObject(_dbuffer2);
	_dbuffer2 = NULL;
	clReleaseMemObject(_dbuffer3);
	_dbuffer3 = NULL;
	clReleaseMemObject(_dbuffer4);
	_dbuffer4 = NULL;
	
	clReleaseCommandQueue(_commandQueue);
	_commandQueue = NULL;
}

Simulator::~Simulator()
{
	release();
}
