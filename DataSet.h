#ifndef __BioSim__DataSet__
#define __BioSim__DataSet__

class VoxelImage;

/**
 * Represents a scanned data set usually loaded from a file. Each data set contains a number of frames, each
 * represented by a \c VoxelImage.
 */
class DataSet {
public:
	/**
	 * Constructs a new \c DataSet. The object takes ownership of the frames.
	 *
	 * @param nFrames  The number of frames.
	 * @param frames   A pointer to the frames.
	 */
	DataSet(int nFrames, VoxelImage **frames);
	
	/**
	 * Returns the number of frames.
	 *
	 * @return The number of frames.
	 */
	int nFrames() const;
	
	/**
	 * Returns a pointer to the specified frame.
	 *
	 * @param frame  The frame number.
	 *
	 * @return The frame.
	 */
	VoxelImage *frame(int frame) const;
	
	/**
	 * Creates a \c DataSet from a file.
	 *
	 * @param filename  The filename to read from.
	 *
	 * @return A \c DataSet containing the contents of the file or \c NULL if it could
	 * not be read.
	 */
	static DataSet *readFromFile(const char *filename);
	
	~DataSet();
	
private:
	DataSet(const DataSet &);
	DataSet &operator=(const DataSet &);

	int _nFrames;
	VoxelImage **_frames;
};

#endif /* defined(__BioSim__DataSet__) */
