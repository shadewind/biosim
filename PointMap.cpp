#include "PointMap.h"

#include "PointData.h"

#define BIN_INDEX(vec) (((vec).z * _nBins.x * _nBins.y) + ((vec).y * _nBins.x) + (vec).x)

PointMap::PointMap(const PointData *data, float binSize)
	: _data(data), _binSize(binSize)
{
	int nPoints = _data->nPoints();
	// First calculate extents.
	glm::vec3 minp(INFINITY, INFINITY, INFINITY);
	glm::vec3 maxp(-INFINITY, -INFINITY, -INFINITY);
	for (int i = 0; i < nPoints; i++) {
		glm::vec4 position = _data->pointAt(i)->state.position;
		minp = glm::min(minp, glm::vec3(position));
		maxp = glm::max(maxp, glm::vec3(position));
	}
	_origin = minp;
	_size = maxp - minp;
	_nBins = glm::max((glm::ivec3)(_size / glm::vec3(binSize)), glm::ivec3(1));

	_bins = std::vector<std::vector<int> >(_nBins.x * _nBins.y * _nBins.z);
	for (int i = 0; i < nPoints; i++) {
		glm::vec4 pos = _data->pointAt(i)->state.position;
		_bins[BIN_INDEX(indexFor(pos))].push_back(i);
	}
}

glm::ivec3 PointMap::indexFor(const glm::vec4 &pos) const
{
	return glm::clamp(
		(glm::ivec3)(glm::vec3(pos) / _binSize),
		glm::ivec3(0), _nBins);
}

const std::vector<int> &PointMap::binFor(const glm::ivec3 &index) const
{
	return _bins[BIN_INDEX(index)];
}

const std::vector<int> &PointMap::binFor(const glm::vec4 &pos) const
{
	return binFor(indexFor(pos));
}

glm::ivec3 PointMap::nBins() const
{
	return _nBins;
}

float PointMap::binSize() const
{
	return _binSize;
}