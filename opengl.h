#ifndef BioSim_opengl_h
#define BioSim_opengl_h_

#ifdef __APPLE__
#	include <OpenGL/OpenGL.h>
#	include <OpenGL/gl3.h>
#else
	//We currently only use GLEW on non-Apple platforms since
	// GLEW is broken on OS X when using OpenCL right now.
#	include <GL/glew.h>
#	ifdef _WIN32
#		include <windows.h>
#	else
#		include <GL/glx.h>
#	endif
#endif

#endif /* BioSim_opengl_h_ */
