#include "utils.h"

#include <cstdio>
#include <cstring>

#include <log.h>

#include <GL/glfw.h>

bool loadResource(const char *name, char **outBuf, size_t *outLen)
{
	static const char prefix[] = "../";
	char *filename = new char[sizeof(prefix) + strlen(name)];
	strcpy(filename, prefix);
	strcat(filename, name);
	bool ret = loadContentsOfFile(filename, outBuf, outLen);
	delete[] filename;
	return ret;
}

bool loadContentsOfFile(const char *filename, char **outBuf, size_t *outLen)
{
	FILE *file = fopen(filename, "rb");
	if (file == NULL)
		return false;
	
	fseek(file, 0, SEEK_END);
	long size = ftell(file);
	fseek(file, 0, SEEK_SET);
	
	char *buf = new char[size];
	fread(buf, sizeof(char), size, file);
	fclose(file);
	
	*outBuf = buf;
	*outLen = size;
	return true;
}

double getSystemTime()
{
	return glfwGetTime();
}