#version 330

in vec4 vertexPosition;
in float vertexIntensity;
in float vertexSelectance;

out vec4 position;
out vec4 color;

uniform mat4 projectionMatrix;
uniform mat4 modelviewMatrix;
uniform float contrast;
uniform bool fog;

void main()
{
	gl_Position = projectionMatrix * modelviewMatrix * vertexPosition;
	position = modelviewMatrix * vertexPosition;
	vec4 hue = vec4(
		1.0f,
		1.0f - vertexSelectance,
		1.0f - vertexSelectance,
		1.0f);

	if (fog)
		color = (hue * pow(vertexIntensity, contrast)) / (position.z * -5.0f);
	else
		color = hue * pow(vertexIntensity, contrast);
}