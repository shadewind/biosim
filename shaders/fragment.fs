#version 330

in vec4 position;
in vec4 color;

out vec4 fragmentColor;

void main()
{
	fragmentColor = color;
}