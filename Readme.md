biosim
==================

Building
-------------

As of now, BioSim looks for kernel and shader sources in the parent directory of the working directory. Because of this, it's recommended to build in a subdirectory of the source root.

Example:

	mkdir build
	cd build
	cmake -D CMAKE_BUILD_TYPE=Debug ..
	make
