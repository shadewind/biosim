#include "PointData.h"

#include <cstdio>
#include <cstring>

#include <arpa/inet.h>

#include <glm/gtx/random.hpp>

#include "log.h"
#include "VoxelImage.h"
#include "Simulator.h"
#include "PointMap.h"

#define POINT_INDEX(x, y, z) (((z) * width * height) + ((y) * width) + (x))

PointData::PointData(int nLinks, int nPoints) : _nLinks(nLinks), _nPoints(nPoints)
{
	calculateStride();
	_data = new uint8_t[_nPoints * _stride];
	memset(_data, 0, _nPoints * _stride);
	clearLinks();
}

PointData::PointData(const VoxelImage *image, int nLinks, float threshold) : _nLinks(nLinks)
{
	int width = image->width();
	int height = image->height();
	int depth = image->depth();

	_nPoints = 0;
	std::vector<uint32_t> indexMap(width * height * depth, NO_POINT);
	
	// First find out how many points we will end up with after filtering
	// and build an index map for them.
	for (int z = 0; z < depth; z++) {
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if (image->voxel(x, y, z)[0] > threshold)
					indexMap[POINT_INDEX(x, y, z)] = _nPoints++;
			}
		}
	}

	calculateStride();

	//Now create the basic data for the points.
	_data = new uint8_t[_stride * _nPoints];
	uint32_t nextPoint = 0;
	for (int z = 0; z < depth; z++) {
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if (indexMap[POINT_INDEX(x, y, z)] != NO_POINT) {
					PointElement *p = pointAt(nextPoint);
					
					p->state.position.x = x - (width / 2.0f);
					p->state.position.y = y - (height / 2.0f);
					p->state.position.z = z - (depth / 2.0f);
					p->state.position.w = 1.0f;
					
					p->state.velocity = glm::vec4(0.0f);

					p->intensity = image->voxel(x, y, z)[0];
					
					Link *links = linksAt(nextPoint);
					for (int l = 0; l < nLinks; l++) {
						links[l].partnerIndex = NO_POINT;
						links[l].restLength = 0.0f;
					}
					
					nextPoint++;
				}
			}
		}
	}
}

PointData::PointData(void *data, int nLinks, int nPoints)
	: _data((uint8_t *)data), _nLinks(nLinks), _nPoints(nPoints)
{
	calculateStride();
}

void PointData::autoLink(float radius)
{
	PointMap map(this, 2.0f);
	int binRadius = (radius / map.binSize()) + 1;
	for (int pointIndex = 0; pointIndex < _nPoints; pointIndex++) {
		if ((pointIndex % 1000) == 0)
			LOG("INFO: Linking point %d...", pointIndex);
		glm::ivec3 binIndex = map.indexFor(pointAt(pointIndex)->state.position);
		glm::ivec3 start = glm::max(binIndex - glm::ivec3(binRadius), glm::ivec3(0));
		glm::ivec3 end = glm::min(binIndex + glm::ivec3(binRadius), map.nBins());

		for (int x = start.x; x < end.x; x++) {
			for (int y = start.y; y < end.y; y++) {
				for (int z = start.z; z < end.z; z++) {
					const std::vector<int> &bin = map.binFor(glm::ivec3(x, y, z));
					for (std::vector<int>::const_iterator it = bin.begin(); it != bin.end(); it++) {
						if (pointIndex != *it)
							linkIfCloser(pointIndex, *it, radius);
					}
				}
			}
		}
	}
	LOG("INFO: Removing unidirectionals...");
	removeUnidirectionals();
	rest();
}

// Adds a link to the point if it has free links or if the neighbor
// is closer than any of the existing links. In the latter case,
// the link replaces the farthest link. The point has to be closer
// than radius.
void PointData::linkIfCloser(int pointIndex, int neighborIndex, float radius)
{
	//Calculate distance^2 instead, it's faster. We'll calculate the real distance later.
	glm::vec4 diff = pointAt(pointIndex)->state.position - pointAt(neighborIndex)->state.position;
	float pseudoDistance = glm::dot(diff, diff);
	if (pseudoDistance > (radius * radius))
		return;
		
	float farthestDistance = 0.0f;
	int farthestLink = -1;
	Link *links = linksAt(pointIndex);
	for (int i = 0; i < _nLinks; i++) {
		if (links[i].partnerIndex == NO_POINT) {
			links[i].partnerIndex = neighborIndex;
			links[i].restLength = pseudoDistance;
			return;
		} else if (links[i].restLength > farthestDistance) {
			farthestDistance = links[i].restLength;
			farthestLink = i;
		}
	}

	if (pseudoDistance < farthestDistance) {
		links[farthestLink].partnerIndex = neighborIndex;
		links[farthestLink].restLength = pseudoDistance;
	}
}

void PointData::removeUnidirectionals()
{
	for (int p = 0; p < _nPoints; p++) {
		Link *links = linksAt(p);
		for (int l = 0; l < _nLinks; l++) {
			if ((links[l].partnerIndex != NO_POINT) && !hasLink(links[l].partnerIndex, p)) {
				links[l].partnerIndex = NO_POINT;
				links[l].restLength = 0.0f;
			}
		}
	}
}

bool PointData::hasLink(int pointIndex, int neighborIndex)
{
	Link *links = linksAt(pointIndex);
	for (int l = 0; l < _nLinks; l++)
	{
		if (links[l].partnerIndex == neighborIndex)
			return true;
	}

	return false;
}

void PointData::calculateStride()
{
	_stride = sizeof(PointElement) + (sizeof(Link) * _nLinks);
	static const size_t alignment = sizeof(float) * 4;
	if ((_stride % alignment) != 0)
		_stride = ((_stride / alignment) + 1) * alignment;
}

std::vector<int> PointData::linkageHistogram() const
{
	std::vector<int> histogram(_nLinks + 1, 0);
	for (int i = 0; i < _nPoints; i++) {
		const Link *links = linksAt(i);
		int linkage = 0;
		for (int l = 0; l < _nLinks; l++) {
			if (links[l].partnerIndex != NO_POINT)
				linkage++;
		}
		histogram[linkage]++;
	}

	return histogram;
}

void PointData::scramble(const glm::vec3 &magnitude)
{
	for (int i = 0; i < _nPoints; i++) {
		pointAt(i)->state.position += glm::vec4(glm::compRand3(-0.5f, 0.5f) * magnitude, 0.0f);
	}
}

void PointData::rest()
{
	for (int i = 0; i < _nPoints; i++) {
		PointElement *point = pointAt(i);
		Link *links = linksAt(i);
		for (int l = 0; l < _nLinks; l++) {
			if (links[l].partnerIndex == NO_POINT)
				continue;
			PointElement *partnerPoint = pointAt(links[l].partnerIndex);
			links[l].restLength = glm::distance(
				point->state.position,
				partnerPoint->state.position);
		}
	}
}

void PointData::removeLinks(int point, int link)
{
	Link *links = linksAt(point);
	for (int l = 0; l < _nLinks; l++) {
		if (links[l].partnerIndex == link) {
			links[l].partnerIndex = NO_POINT;
			links[l].restLength = 0.0f;
		}
	}
}

bool PointData::saveToFile(const char *filename) const
{
	FILE *file = fopen(filename, "w");
	if (file == NULL)
		return false;

	// First 32 bits is number of links.
	uint32_t nLinks = htonl(_nLinks);
	fwrite(&nLinks, sizeof(uint32_t), 1, file);


	//Next, write point elements together with their links, packed.
	for (int i = 0; i < _nPoints; i++) {
		//First the point element itself
		fwrite(pointAt(i), sizeof(PointElement), 1, file);
		//Then each link
		const Link *links = linksAt(i);
		for (int l = 0; l < _nLinks; l++) {
			Link link = links[l];
			//Partner index should be in network byte order.
			link.partnerIndex = htonl(link.partnerIndex);
			fwrite(&link, sizeof(Link), 1, file);
		}
	}

	fclose(file);
	return true;
}

PointData *PointData::loadFromFile(const char *filename)
{
	FILE *file = fopen(filename, "r");
	if (file == NULL)
		return NULL;

	// Find the length of the file to find out the number of points.
	fseek(file, 0, SEEK_END);
	long size = ftell(file);
	fseek(file, 0, SEEK_SET);

	uint32_t nLinks;
	fread(&nLinks, sizeof(uint32_t), 1, file);
	nLinks = ntohl(nLinks);

	//Find out the size of a packed element.
	size_t elementSize = sizeof(PointElement) + (sizeof(Link) * nLinks);
	int nPoints = (size - sizeof(uint32_t)) / elementSize;

	PointData *data = new PointData(nLinks, nPoints);
	for (int i = 0; i < nPoints; i++) {
		fread(data->pointAt(i), sizeof(PointElement), 1, file);
		Link *links = data->linksAt(i);
		for (int l = 0; l < nLinks; l++) {
			Link link;
			fread(&link, sizeof(Link), 1, file);
			link.partnerIndex = ntohl(link.partnerIndex);
			links[l] = link;
		}
	}

	fclose(file);
	return data;
}

void PointData::clearLinks()
{
	for (int i = 0; i < _nPoints; i++) {
		Link *links = linksAt(i);
		for (int l = 0; l < _nLinks; l++) {
			links[l].partnerIndex = NO_POINT;
			links[l].restLength = 0.0f;
		}
	}
}

void PointData::recursiveSelect(const glm::vec4 &origin, float radius, int index)
{
	if (index == NO_POINT)
		return;

	PointElement *point = pointAt(index);
	if (point->selectance != 0.0f)
		return;

	float distance = glm::distance(point->state.position, origin);
	if (distance < radius) {
		float x = distance / radius;
		point->selectance = 1.0f - (x * x);
		Link *links = linksAt(index);
		for (int l = 0; l < _nLinks; l++)
			recursiveSelect(origin, radius, links[l].partnerIndex);
	}
}

void PointData::clearSelection()
{
	for (int i = 0; i < _nPoints; i++)
		pointAt(i)->selectance = 0.0f;
}

size_t PointData::stride() const
{
	return _stride;
}

int PointData::nPoints() const
{
	return _nPoints;
}

void *PointData::pointData()
{
	return _data;
}

const void *PointData::pointData() const
{
	return _data;
}

size_t PointData::dataSize() const
{
	return stride() * _nPoints;
}

size_t PointData::padding() const
{
	return stride() - sizeof(PointElement) - (sizeof(Link) * _nLinks);
}

PointData::PointElement *PointData::pointAt(int index)
{
	return (PointElement *)(_data + (index * _stride));
}

PointData::Link *PointData::linksAt(int index)
{
	return (Link *)(_data + (index * _stride) + sizeof(PointElement));
}

const PointData::PointElement *PointData::pointAt(int index) const
{
	return (PointElement *)(_data + (index * _stride));
}

const PointData::Link *PointData::linksAt(int index) const
{
	return (Link *)(_data + (index * _stride) + sizeof(PointElement));
}

int PointData::nLinks() const
{
	return _nLinks;
}

PointData::~PointData()
{
	delete[] _data;
}