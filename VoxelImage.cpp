#include "VoxelImage.h"

#include <cstring>

VoxelImage::VoxelImage(int width, int height, int depth, int numComponents, float *data)
	: _width(width), _height(height), _depth(depth), _numComponents(numComponents), _data(data)
{
	if (_data == NULL) {
		int floatSize = _width * _height * _depth * _numComponents;
		_data = new float[floatSize];
		memset(_data, 0, floatSize * sizeof(float));
	}
}

VoxelImage::VoxelImage(const VoxelImage &other)
	: _width(other._width), _height(other._height), _depth(other._depth), _numComponents(other._numComponents)
{
	int size = _width * _height * _depth * _numComponents;
	_data = new float[size];
	memcpy(_data, other._data, size * sizeof(float));
}

int VoxelImage::width() const
{
	return _width;
}

int VoxelImage::height() const
{
	return _height;
}

int VoxelImage::depth() const
{
	return _depth;
}

int VoxelImage::numComponents() const
{
	return _numComponents;
}

const float *VoxelImage::data() const
{
	return _data;
}

const float *VoxelImage::voxel(int x, int y, int z) const
{
	return _data + (((z * _width * _height) + (y * _width) + x) * _numComponents);
}

float *VoxelImage::voxel(int x, int y, int z)
{
	return _data + (((z * _width * _height) + (y * _width) + x) * _numComponents);
}

size_t VoxelImage::byteSize() const
{
	return _width * _height * _depth * _numComponents * sizeof(float);
}

VoxelImage *VoxelImage::downsampled() const
{
	int newWidth = _width / 2;
	int newHeight = _height / 2;
	int newDepth = _depth / 2;
	float *newData = new float[newWidth * newHeight * newDepth * _numComponents];
	for (int z = 0; z < newDepth; z++) {
		for (int y = 0; y < newHeight; y++) {
			for (int x = 0; x < newWidth; x++) {
				float *dest = newData + (((z * newWidth * newHeight) + (y * newWidth) + x) * _numComponents);
				const float *src[] = {
					voxel((2 * x), (2 * y), (2 * z)),
					voxel((2 * x)+1, (2 * y), (2 * z)),
					voxel((2 * x)+1, (2 * y)+1, (2 * z)),
					voxel((2 * x)+1, (2 * y), (2 * z)+1),
					voxel((2 * x)+1, (2 * y)+1, (2 * z)+1),
					voxel((2 * x), (2 * y)+1, (2 * z)),
					voxel((2 * x), (2 * y), (2 * z)+1),
					voxel((2 * x), (2 * y)+1, (2 * z)+1)
				};
				
				for (int c = 0; c < _numComponents; c++) {
					for (int i = 0; i < 8; i++)
						dest[c] += src[i][c];
					dest[c] /= 8;
				}
			}
		}
	}
	
	return new VoxelImage(newWidth, newHeight, newDepth, _numComponents, newData);
}

VoxelImage *VoxelImage::downsampled(int n) const
{
	VoxelImage *output = new VoxelImage(*this);
	for (int i = 0; i < n; i++)
		output = output->downsampled();
	return output;
}

void VoxelImage::fill(float *values)
{
	int nVoxels = _width * _height * _depth;
	for (int i = 0; i < nVoxels; i++) {
		float *voxel = _data + (i * _numComponents);
		for (int c = 0; c < _numComponents; c++)
			voxel[c] = values[c];
	}
}

VoxelImage::~VoxelImage()
{
	delete[] _data;
}