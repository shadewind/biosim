#ifndef __BioSim__PointData__
#define __BioSim__PointData__

#include <cstddef>
#include <vector>
#include <stdint.h>

#include <glm/glm.hpp>

class VoxelImage;

/**
 * Helper class used by \c Simulator for building the data used for
 * simulation.
 */
class PointData {
public:
	/**
	 * Represent the physics state of a point.
	 */
	struct PhysicsState {
		glm::vec4 position;	///< The position of the point.
		glm::vec4 velocity;	///< The velocity of the point.
	};

	/**
	 * Represents a link.
	 */
	struct Link {
		uint32_t partnerIndex;	///< The index of the parter point.
		float restLength;		///< The rest length of the link.
	};

	/**
	 * Contains information about a point such as its intensity physics state.
	 */
	struct PointElement {
		PhysicsState state;	///< The point physics state.
		float intensity;	///< The point intensity.
		float selectance;   ///< Weird name but this describes "how selected" a point is.
	};

	/**
	 * Constructs a new empty point data.
	 *
	 * @param nLinks   The number of links.
	 * @param nPoints  The number of points.
	 */
	PointData(int nLinks, int nPoints);

	/**
	 * Constructs a new point data from the specified \c VoxelImage.
	 *
	 * @param image      The \c VoxelImage to take point data from.
	 * @param nLinks     The number of links.
	 * @param threshold  The intensity threshold for discarding points.
	 */
	PointData(const VoxelImage *image, int nLinks, float threshold = 0.5f);

	/**
	 * Constructs a new \c PointData from the specified raw point data buffer.
	 *
	 * @param data     A pointer to the data. This object will take ownership of the data.
	 * @param nLinks   The number of links per point.
	 * @param nPoints  The number of points.
	 */
	PointData(void *data, int nLinks, int nPoints);

	/**
	 * Returns the stride in bytes. The stride is automatically calculated for
	 * alignment to sizeof(float) * 4.
	 *
	 * @return The stride.
	 */
	size_t stride() const;

	/**
	 * Returns the actual number of retained points.
	 *
	 * @return The number of points.
	 */
	int nPoints() const;

	/**
	 * Returns the base pointer for the point data.
	 *
	 * @return The point data pointer.
	 */
	void *pointData();
	const void *pointData() const;

	/**
	 * Returns the total size of the data.
	 *
	 * @return The total size of the data in bytes.
	 */
	size_t dataSize() const;

	/**
	 * Returns the amount of data padding added for each element.
	 *
	 * @return The amount of data padding in bytes.
	 */
	size_t padding() const;

	/**
	 * Returns the point with the specified index.
	 *
	 * @return The pointer to the point.
	 */ 
	PointElement *pointAt(int index);
	const PointElement *pointAt(int index) const;

	/**
	 * Returns the links of the point with the specified index.
	 *
	 * @return A pointer to links array.
	 */ 
	Link *linksAt(int index);
	const Link *linksAt(int index) const;

	/**
	 * Automatically adds links for every point. For each point, the closest neighbors are
	 * added as links. Afterwards, all unidirectional links are removed.
	 *
	 * @param radius  The maximum allowed distance between points to link.
	 */
	void autoLink(float radius);

	/**
	 * Removes links that are not bidirectional.
	 */
	void removeUnidirectionals();

	/**
	 * Returns the number of links.
	 *
	 * @return The number of links.
	 */
	int nLinks() const;

	/**
	 * Creates a histogram of the number of links for each point.
	 *
	 * @return A vector containing the histogram.
	 */
	std::vector<int> linkageHistogram() const;

	/**
	 * Adds 3D positional noise to each point.
	 *
	 * @param magnitude  A vector describining the magnitude of the noise along
	 *                   each axis.
	 */
	void scramble(const glm::vec3 &magnitude);

	/**
	 * Sets the rest length of every link to it's current length.
	 */
	void rest();

	/**
	 * Remove any links from the point \c index to the point \c link.
	 *
	 * @param index  The point to remove the links from.
	 * @param link   The linked point.
	 */
	void removeLinks(int point, int link);

	/**
	 * Removes all links for all points.
	 */
	void clearLinks();

	/**
	 * Sets the selectance for every point to 0.0f.
	 */
	void clearSelection();

	/**
	 * Selects points by recursing through links from the specified
	 * point.
	 */
	void recursiveSelect(const glm::vec4 &origin, float radius, int index);

	/**
	 * Saves the data to a file.
	 *
	 * @param filename  The name of the file to save to.
	 *
	 * @return \c true on succes, \c false on failure. 
	 */
	bool saveToFile(const char *filename) const;

	/**
	 * Loads point data from a file.
	 *
	 * @param filename  The name of the file to load from.
	 *
	 * @return A pointer to the loaded point data object or \c NULL on failure.
	 */
	static PointData *loadFromFile(const char *filename);

	~PointData();

private:
	PointData(const PointData &);
	PointData &operator=(const PointData &);

	void linkIfCloser(int pointIndex, int neighborIndex, float radius);
	bool hasLink(int pointIndex, int neighborIndex);
	void calculateStride();

	int _nPoints;
	int _nLinks;
	size_t _stride;
	uint8_t *_data;
};

#endif /* defined(__BioSim__PointData__) */
