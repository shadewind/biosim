#define NO_POINT		(4294967295U)

#define DRAG			2.0f

/**
 * Represent the physics state of a point.
 */
typedef struct {
	float4 position;	///< The position of the point.
	float4 velocity;	///< The velocity of the point.
} PhysicsState;

/**
 * Represents a link.
 */
typedef struct {
	unsigned int partnerIndex;	///< The index of the parter point.
	float restLength;			///< The rest length of the link.
} Link;

/**
 * Contains information about a point such as its intensity and link information.
 */
typedef struct {
	PhysicsState state;		///< The point physics state.
	float intensity;		///< The point intensity.
	float selectance;   	///< Weird name but this describes "how selected" a point is.
	Link links[N_LINKS];	///< The point links.
	char padding[PADDING];
} PointElement;

/**
 * Calculates the spring-damper force acting on p1 for the spring between p1 and p2.
 *
 * @param p1          The first point.
 * @param p2          The second point.
 * @param restLength  The rest length of the spring.
 *
 * @return The force.
 */
float4 calculateSpringForce(PhysicsState p1, PhysicsState p2, float restLength);

/**
 *
 */
float4 transform4(float4 m0, float4 m1, float4 m2, float4 m3, float4 vec);

/**
 * Returns true if the specified point is inside the specified cone.
 */
bool insideCone(float4 origin, float4 dir, float radius, float4 point);

/**
 * Calculates the weighted average for the specified derivatives and integrates each point.
 *
 * @param points   The global point buffer.
 * @param d1       The derivative #1.
 * @param d2       The derivative #2.
 * @param d3       The derivative #3.
 * @param d4       The derivative #4.
 * @param nPoints  The number of points.
 * @param dt       The time delta.
 */
kernel void integrate(
	global PointElement *points,
	global PhysicsState *d1,
	global PhysicsState *d2,
	global PhysicsState *d3,
	global PhysicsState *d4,
	int nPoints,
	float dt)
{
	int id = get_global_id(0);
	if (id >= nPoints)
		return;
	
	PointElement point = points[id];
	PhysicsState pd1 = d1[id];
	PhysicsState pd2 = d2[id];
	PhysicsState pd3 = d3[id];
	PhysicsState pd4 = d4[id];
	
	PhysicsState derivative;
	derivative.position = (pd1.position + (2.0f * pd2.position) + (2.0f * pd3.position) + pd4.position) / 6.0f;
	derivative.velocity = (pd1.velocity + (2.0f * pd2.velocity) + (2.0f * pd3.velocity) + pd4.velocity) / 6.0f;
	
	PhysicsState newState;
	newState.position = point.state.position + (derivative.position * dt);
	newState.velocity = point.state.velocity + (derivative.velocity * dt);
	points[id].state = newState;
}

/**
 * Calculates the derivatives for all points after time \c dt has passed assuming the specified initial derivatives.
 *
 * @param points           The global point data.
 * @param initDerivatives  The initial derivatives to assume or \c NULL.
 * @param result           The resulting derivatives.
 * @param nPoints          The number of points.
 * @param dt               The time delta.
 */
kernel void evaluate(
	global PointElement *points,
	global PhysicsState *initDerivatives,
	global PhysicsState *result,
	int nPoints,
	float dt,
	float t)
{
	int id = get_global_id(0);
	if (id >= nPoints)
		return;
	PointElement point = points[id];

	PhysicsState futureState;
	futureState.position = point.state.position;
	futureState.velocity = point.state.velocity;
	
	if (initDerivatives != 0) {
		PhysicsState initDerivative = initDerivatives[id];
		futureState.position += initDerivative.position * dt;
		futureState.velocity += initDerivative.velocity * dt;
	}
	
	float4 force = (float4)(0.0f);
#ifdef TEST_FORCE
	if ((futureState.position.x > 0.0f) && (futureState.position.y > 0.0f))
		force += (float4)(0.0f, 10.0f, 10.0f, 0.0f);
#endif
	force += -futureState.velocity * DRAG;
	
	for (int i = 0; i < N_LINKS; i++) {
		unsigned int partnerIndex = point.links[i].partnerIndex;
		if (partnerIndex != NO_POINT) {
			PointElement partner = points[partnerIndex];
			PhysicsState partnerFutureState = partner.state;
			if (initDerivatives != 0) {
				PhysicsState partnerDerivative = initDerivatives[partnerIndex];
				partnerFutureState.position += partnerDerivative.position * dt;
				partnerFutureState.velocity += partnerDerivative.velocity * dt;
			}
			
			force += calculateSpringForce(futureState, partnerFutureState, point.links[i].restLength);
		}
	}
	
	PhysicsState derivative;
	//Don't apply force on selected points.
	derivative.position = futureState.velocity * (1.0f - point.selectance);
	derivative.velocity = force * (1.0f - point.selectance);
	result[id] = derivative;
}

/**
 * Calculates the spring-damper force between the two specified with regards to the first point.
 *
 * @param p1          The first point.
 * @param p2          The second point.
 * @param restLength  The spring rest length.
 *
 * @return The calculated force.
 */
float4 calculateSpringForce(PhysicsState p1, PhysicsState p2, float restLength)
{
	float ks = SPRING_SCALE;
	float kd = DAMPER_SCALE;
	
	float4 forceDirection = normalize(p2.position - p1.position);
	
	float springForce = (distance(p2.position, p1.position) - restLength) * ks;

	float damperForce = (dot(forceDirection, p2.velocity) - dot(forceDirection, p1.velocity)) * kd;
	
	return forceDirection * (damperForce + springForce);
}

/**
 * Multiplies the specified vector with the specified matrix. The matrix is specified
 * as four column vectors.
 *
 * @param m0   The first column vector.
 * @param m1   The second column vector.
 * @param m2   The third column vector.
 * @param m3   The fourth column vector.
 * @param vec  The vector.
 */
 float4 transform4(float4 m0, float4 m1, float4 m2, float4 m3, float4 vec)
 {
 	return (float4)(
 		(m0.x * vec.x) + (m1.x * vec.y) + (m2.x * vec.z) + (m3.x * vec.w),
 		(m0.y * vec.x) + (m1.y * vec.y) + (m2.y * vec.z) + (m3.y * vec.w),
 		(m0.z * vec.x) + (m1.z * vec.y) + (m2.z * vec.z) + (m3.z * vec.w),
 		(m0.w * vec.x) + (m1.w * vec.y) + (m2.w * vec.z) + (m3.w * vec.w));
 }

/**
 * Transforms all selected points. Their velocity will also be reduced according to the selectance.
 *
 * @param points   The global point data.
 * @param nPoints  The total number of points.
 * @param m0       The first column vector of the transform matrix.
 * @param m1       The second column vector of the transform matrix.
 * @param m2       The third column vector of the transform matrix.
 * @param m3       The fourth column vector of the transform matrix.
 */
 kernel void transformSelection(
 	global PointElement *points,
 	int nPoints,
 	float4 m0, float4 m1, float4 m2, float4 m3)
 {
 	int id = get_global_id(0);
	if (id >= nPoints)
		return;
	PointElement point = points[id];
	float4 transformed = transform4(m0, m1, m2, m3, point.state.position);
	//Interpolate based on selectance.
	point.state.position = (point.state.position * (1.0f - point.selectance)) + (transformed * point.selectance);
	point.state.velocity *= (1.0f - point.selectance);
	points[id] = point;
 }

/**
 * Removes links for each point which falls inside the specified cone.
 * Points which fall inside the cone also have their intensity set to 0.0f.
 *
 * @param points     A pointer to the point array.
 * @param nPoints    The total number of points.
 * @param origin     The cone origin.
 * @param direction  The cone direction.
 * @param radius     The cone radius at a distance of 1.
 */
kernel void clearConeLinks(
	global PointElement *points,
	int nPoints,
	float4 origin,
	float4 direction,
	float radius)
{
	int id = get_global_id(0);
	if (id >= nPoints)
		return;

	PointElement point = points[id];
	if (insideCone(origin, direction, radius, point.state.position)) {
		// The point is inside the cone, clear all links.
		for (int i = 0; i < N_LINKS; i++)
			point.links[i].partnerIndex = NO_POINT;
		point.intensity = 0.0f;
	} else {
		// The point is not inside the cone, check if linked points are inside of the cone
		// and remove the corresponding links.
		for (int i = 0; i < N_LINKS; i++) {
			unsigned int partnerIndex = point.links[i].partnerIndex;
			if (partnerIndex == NO_POINT)
				continue;
			float4 partnerPosition = points[partnerIndex].state.position;
			if (insideCone(origin, direction, radius, partnerPosition))
				point.links[i].partnerIndex = NO_POINT;
		}
	}

	points[id] = point;
}

bool insideCone(float4 origin, float4 dir, float radius, float4 point)
{
	float dp = dot(point - origin, dir);
	if (dp < 0.0f)
		return false;
	float dist = distance(point, origin + (dir * dp));
	return dist < (dp * radius);
}