#!/usr/bin/env ruby

require "googlecharts"

P0 = 5.0
V0 = 0.0
SPRING_K = 45.0
DAMPER_C = 3.0
TIMELIMIT = 10.0

State = Struct.new("State", :position, :velocity, :time)
Test = Struct.new("Test", :name, :dt, :method)

def force(state)
	(state.position * -SPRING_K) + (state.velocity * -DAMPER_C)
end

Euler = Proc.new do |states, dt|
	State.new(
		states[-1].position + (states[-1].velocity * dt),
		states[-1].velocity + (force(states[-1]) * dt),
		states[-1].time + dt)
end

def rk4_eval(init_state, v, a, dt)
	state = init_state.dup
	unless dt.nil?
		state.position += v * dt
		state.velocity += a * dt
	end

	return state.velocity, force(state)
end

Rk4 = Proc.new do |states, dt|
	s = states[-1]

	v1, a1 = rk4_eval(s, nil, nil, nil)
	v2, a2 = rk4_eval(s, v1, a1, dt / 2)
	v3, a3 = rk4_eval(s, v2, a2, dt / 2)
	v4, a4 = rk4_eval(s, v3, a3, dt)

	State.new(
		s.position + dt * ((v1 + 2*v2 + 2*v3 + v4) / 6),
		s.velocity + dt * ((a1 + 2*a2 + 2*a3 + a4) / 6),
		s.time + dt)
end

Verlet = Proc.new do |states, dt|
	s = states[-1]
	sp = states[-2]

	mock_state = State.new(
		s.position,
		(s.position - sp.position) / (s.time - sp.time),
		s.time)

	State.new(
		(s.position * 2) - sp.position + (dt * dt * force(mock_state)),
		nil,
		s.time + dt)
end

def integrate(dt, method)
	states = [ State.new(P0, V0, -dt), State.new(P0, V0, 0.0) ]
	while states.last.time < TIMELIMIT
		states << method[states, dt]
	end

	states[1..-1]
end

def make_plot(tests, filename)
	IO.popen("gnuplot", "w") do |io|
		io.puts "set terminal postscript eps color"
		io.puts "set output \"#{filename}\""
		io.puts "set yrange [#{-P0}:#{P0}]"

		io.puts "r1 = (3.0/2.0)*(-1-sqrt(19.0)*{0,1})"
		io.puts "r2 = (3.0/2.0)*(-1+sqrt(19.0)*{0,1})"
		io.puts "c1 = 5.0*({0,1}+sqrt(19.0))/(2*sqrt(19.0))"
		io.puts "c2 = 5.0/2.0-{0,5}/(2*sqrt(19.0))"
		io.puts "x(t) = c1*exp(r1*t) + c2*exp(r2*t)"
		
		io.puts "set linestyle 1 lt 1 lw 4 lc rgb 'black'"
		io.puts "set linestyle 2 lt 5 lw 4 lc rgb 'blue'"
		io.puts "set linestyle 3 lt 5 lw 4 lc rgb 'red'"
		io.puts "set linestyle 4 lt 5 lw 4 lc rgb 'violet'"
		io.puts "set linestyle 5 lt 5 lw 4 lc rgb 'dark-green'"

		plots = "real(x(t)) ls 1 title 'Exact', "
		plots << tests.each_with_index.map do |t,i|
			"'-' with lines ls #{i + 2} title '#{t.name}, h = #{t.dt}'"
		end.to_a.join(",")
		io.puts "plot [t=0:#{TIMELIMIT}] #{plots}"

		tests.each do |test|
			integrate(test.dt, test.method).each { |s| io.puts "#{s.time} #{s.position}" }
			io.puts "e"
		end
	end
end

timesteps = [ 0.001, 0.01, 0.1, 0.4 ]

euler_tests = timesteps.map { |t| Test.new("Euler", t, Euler) }
rk4_tests = timesteps.map { |t| Test.new("Rk4", t, Rk4) }
verlet_tests = timesteps.map { |t| Test.new("Verlet", t, Verlet) }

make_plot(euler_tests, "output/euler.eps")
make_plot(rk4_tests, "output/rk4.eps")
make_plot(verlet_tests, "output/verlet.eps")