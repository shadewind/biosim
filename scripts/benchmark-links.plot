set terminal postscript eps color
set output "output/benchmark-links.eps"
set xlabel "Number of links"
set ylabel "Execution time (s)"

plot "output/benchmark-links.txt" with linespoints