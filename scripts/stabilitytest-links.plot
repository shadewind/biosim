set terminal postscript eps color
set output "output/stabilitytest-links.eps"
set xlabel "Time step size (s)"
set ylabel "Number of links"
plot "output/stabilitytest-links.txt" with linespoints notitle