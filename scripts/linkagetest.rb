#!/usr/bin/env ruby

require "open3"

EXECUTABLE = "../build/biosim"

def test_linkage(args)
	output, status  = Open3.capture2e("#{EXECUTABLE} #{args.join(' ')}")
	histogram = {}
	output.each_line do |line|
			histogram[$1.to_i] = $2.to_i if line =~ /Linkage = (\d+): (\d+)/
	end
	histogram.to_a.sort_by { |key, value| key }.map { |key, value| value }
end

ARGUMENTS = [
	"--cube=30x30x30",
	"--time-limit=0",
	"-t", "0.01",
	"-l", "10"
]

File.open("output/linkage.txt", "w") do |file|
	(0.0..1.0).step(0.1).reverse_each do |noise|
		histogram = test_linkage(ARGUMENTS + [ "--noise", noise.to_s ])
		puts "noise=#{noise}: #{histogram.join(' ')}"
		histogram.each_with_index do |v, i|
			file.puts "#{i} #{noise} #{v}"
		end
		file.puts
	end
end