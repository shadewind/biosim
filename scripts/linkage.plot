set terminal postscript eps color
set output "output/linkage.eps"
set xlabel "Number of links"
set ylabel "Noise magnitude"
set zlabel "Number of points"
splot "output/linkage.txt" with lines notitle