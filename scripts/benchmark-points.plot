set terminal postscript eps color
set output "output/benchmark-points.eps"
set ylabel "Execution time (s)"
set xlabel "Number of points"
set key autotitle columnhead
plot for[i=0:4] \
	"output/benchmark-points.txt" index i \
	with linespoints lw 4