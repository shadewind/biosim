#!/usr/bin/env ruby

# Stability test script.

EXECUTABLE = "../build/biosim"
ARGUMENTS = [
	"--cube=10x10x10",
	"--time-limit=2.0",
	"--detect-explosion",
	"--test-force",
	"--no-realtime"
]

SPRING_SCALE_TIMESTEP_RANGE = 0.001..0.02
SPRING_SCALE_TIMESTEP_STEP = 0.0005
SPRING_SCALE_START = 1.0
SPRING_SCALE_EXPONENT = 1.1

def test_timestep_springk(timestep, start)
	spring_scale = start
	loop do
		puts "t=#{timestep} k=#{spring_scale}"
		`#{EXECUTABLE} #{ARGUMENTS.join(' ')} -t #{timestep} -S #{spring_scale} -D 2 2> /dev/null`
		return spring_scale unless $?.exitstatus == 0
		spring_scale *= SPRING_SCALE_EXPONENT
	end
end

LINKS_START = 2
LINKS_STEP = 2
LINKS_TIMESTEP_RANGE = 0.006..0.02
LINKS_TIMESTEP_STEP = 0.0005

def test_timestep_links(timestep, start)
	nlinks = start
	loop do
		puts "t=#{timestep} nlinks=#{nlinks}"
		`#{EXECUTABLE} #{ARGUMENTS.join(' ')} -t #{timestep} -l #{nlinks} -S 10000 2> /dev/null`
		return nlinks unless $?.exitstatus == 0
		nlinks += 2
	end
end

case ARGV[0]
when "springk"
	start = SPRING_SCALE_START
	File.open("output/stabilitytest-sprink.txt", "w") do |file|
		SPRING_SCALE_TIMESTEP_RANGE.step(SPRING_SCALE_TIMESTEP_STEP).reverse_each do |timestep|
			start = test_timestep_springk(timestep, start)
			file.puts "#{timestep} #{start}"
		end
	end
when "links"
	start = LINKS_START
	File.open("output/stabilitytest-links.txt", "w") do |file|
		LINKS_TIMESTEP_RANGE.step(LINKS_TIMESTEP_STEP).reverse_each do |timestep|
			start = test_timestep_links(timestep, start)
			file.puts "#{timestep} #{start}"
		end
	end
end