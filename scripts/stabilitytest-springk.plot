set terminal postscript eps color
set output "output/stabilitytest-springk.eps"
set xlabel "Time step size (s)"
set ylabel "Spring constant"
plot "output/stabilitytest-sprink.txt" with linespoints notitle