#!/usr/bin/env ruby

EXECUTABLE = "../build/biosim"
NSTEPS = 1000
ARGUMENTS = [ "-t", "0.001" ]


def benchmark(args)
	all_args = ARGUMENTS + args + [ "--benchmark=#{NSTEPS}" ]
	output = `#{EXECUTABLE} #{all_args.join(" ")}`
	puts output
	output[/time=([0-9\.]+)/, 1].to_f
end

POINT_TEST_RANGE = 10000..100000
POINT_TEST_STEP = 10000
POINT_TEST_NLINKS = [ 6, 8, 10, 12, 14 ]

def points_test
	File.open("output/benchmark-points.txt", "w") do |file|
		POINT_TEST_NLINKS.each do |nlinks|
			file.puts "\"Time\" \"#{nlinks} links\""
			POINT_TEST_RANGE.step(POINT_TEST_STEP).map do |n|
				puts "points=#{n} links=#{nlinks}"
				time = benchmark([ "--cube=10x10x#{n / 100}", "-l", nlinks.to_s ])
				result = "#{n} #{time}"
				file.puts result
				puts result 
			end
			file.puts
			file.puts
		end
	end
end

LINKSTEST_RANGE = 2..40
LINKTEST_STEP = 2

def links_test
	File.open("output/benchmark-links.txt", "w") do |file|
		LINKSTEST_RANGE.step(LINKTEST_STEP) do |nlinks|
			puts "links=#{nlinks}"
			time = benchmark([ "--cube=30x30x30", "-l", nlinks.to_s ])
			result = "#{nlinks} #{time}"
			file.puts result
			puts result
		end
	end
end

case ARGV[0]
when "points" then points_test
when "links" then links_test
end