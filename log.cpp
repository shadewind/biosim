#include "log.h"

#include <cstdio>
#include <cstdarg>

void doLog(const char *format, ...)
{
	va_list ap;
	va_start(ap, format);
	vfprintf(stderr, format, ap);
	va_end(ap);
	fprintf(stderr, "\n");
}