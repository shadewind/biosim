#ifndef BioSim_opencl_h
#define BioSim_opencl_h_

#ifdef __APPLE__
#	include <OpenCL/OpenCL.h>
#else
#	include <CL/cl.h>
#	include <CL/cl_gl.h>
#	include <CL/cl_ext.h>
#endif

#endif /* BioSim_opencl_h_ */
