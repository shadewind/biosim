#include <cstdio>
#include <cstring>

#include <getopt.h>

#include "opengl.h"
#include <GL/glfw.h>

#include <glm/glm.hpp>

#include "opencl.h"
#include "log.h"
#include "Simulator.h"
#include "Renderer.h"
#include "VoxelImage.h"
#include "DataSet.h"

static bool initialize();
static PointData *createData();
static void run();
static void runBenchmark();
static void cleanup();
static void updateTitle();
static glm::vec2 toCameraSpace(int x, int y);
static void updateSelection();

static void GLFWCALL windowResized(int width, int height);
static void GLFWCALL mouseButtonChanged(int button, int action);
static void GLFWCALL mouseMoved(int x, int y);
static void GLFWCALL mouseWheelMoved(int pos);
static void GLFWCALL keyChanged(int key, int action);
static void mouseDragged(int x, int y, int dx, int dy);

static void errorCallback(const char *errInfo, const void *privateInfo, size_t cb, void *userData);
static cl_context createClContext();
static bool parseOptions(int argc, char **argv);
static void printUsage();

// Option parsing

static struct {
	bool useCube;
	int cubeWidth, cubeHeight, cubeDepth;
	float noise;
	const char *filename;
	int frame;
	float timestep;
	float intensityThreshold;
	bool detectExplosion;
	float springScale;
	float damperScale;
	float timeLimit;
	const char *saveFile;
	int nLinks;
	int benchmark;
	int downsample;
	float linkRadius;
	bool testForce;
	bool realtime;
	bool noQuit;
} options;

static const char optstring[] = "f:t:T:S:D:L:Xl:";

static const struct option longopts[] = {
	{ "cube", required_argument, NULL, 0 },
	{ "noise", required_argument, NULL, 0 },
	{ "frame", required_argument, NULL, 'f' },
	{ "timestep", required_argument, NULL, 't' },
	{ "threshold", required_argument, NULL, 'T' },
	{ "detect-explosion", no_argument, NULL, 'X' },
	{ "spring-scale", required_argument, NULL, 'S' },
	{ "damper-scale", required_argument, NULL, 'D' },
	{ "time-limit", required_argument, NULL, 'L' },
	{ "save", required_argument, NULL, 0 },
	{ "nlinks", required_argument, NULL, 'l' },
	{ "benchmark", required_argument, NULL, 0 },
	{ "downsample", required_argument, NULL, 0 },
	{ "link-radius", required_argument, NULL, 0 },
	{ "test-force", no_argument, NULL, 0 },
	{ "no-realtime", no_argument, NULL, 0 },
	{ "no-quit", no_argument, NULL, 0 },
	{ 0, 0, NULL, 0}
};

static Simulator *simulator = NULL;
static bool simulationPaused = false;
static Renderer *renderer = NULL;
static cl_context clContext = NULL;

// Input state variables
static bool mouseDrag = false;
static int mouseDragButton;
static int mouseDragStartX, mouseDragStartY;
static int mouseLastPosX, mouseLastPosY;
static int mouseWheelLastPos;

// Manipulation tools.
enum Tool {	TOOL_MOVE, TOOL_CUT };
static Tool currentTool = TOOL_MOVE;
static float selectionRadius = 5.0f;
static bool rebuildLinks = false;

bool parseOptions(int argc, char **argv)
{
	//Set defaults
	options.useCube = false;
	options.noise = 1.0f;
	options.filename = NULL;
	options.frame = 0;
	options.timestep = 0.02f;
	options.intensityThreshold = 0.5f;
	options.detectExplosion = false;
	options.springScale = 1000.0f;
	options.damperScale = 15.0f;
	options.timeLimit = INFINITY;
	options.saveFile = NULL;
	options.nLinks = 8;
	options.benchmark = 0;
	options.downsample = 0;
	options.linkRadius = 2.0f;
	options.testForce = false;
	options.realtime = true;
	options.noQuit = false;

	char ch;
	int li;
	while ((ch = getopt_long(argc, argv, optstring, longopts, &li)) != -1) {
		switch(ch) {
		case 0:
			if (strcmp("cube", longopts[li].name) == 0) {
				options.useCube = true;
				if (sscanf(optarg, "%dx%dx%d", &options.cubeWidth, &options.cubeHeight, &options.cubeDepth) != 3) {
					LOG("ERROR: --cube argument '%s' is not valid.", optarg);
					return false;
				}
			} else if (strcmp("noise", longopts[li].name) == 0) {
				if (sscanf(optarg, "%f", &options.noise) != 1) {
					LOG("ERROR: --noise argument '%s' is not a valid decimal.");
					return false;
				}
			} else if (strcmp("save", longopts[li].name) == 0) {
				options.saveFile = optarg;
			} else if (strcmp("benchmark", longopts[li].name) == 0) {
				if (sscanf(optarg, "%d", &options.benchmark) != 1) {
					LOG("ERROR: --benchmark argument is not a valid number.");
					return false;
				}
			} else if (strcmp("downsample", longopts[li].name) == 0) {
				if (sscanf(optarg, "%d", &options.downsample) != 1) {
					LOG("ERROR: --downsample argument is not a valid number.");
					return false;
				}
			} else if (strcmp("link-radius", longopts[li].name) == 0) {
				if (sscanf(optarg, "%f", &options.linkRadius) != 1) {
					LOG("ERROR: --link-radius argument is not a valid decimal.");
					return false;
				}
			} else if (strcmp("test-force", longopts[li].name) == 0) {
				options.testForce = true;
			} else if (strcmp("no-realtime", longopts[li].name) == 0) {
				options.realtime = false;
			} else if (strcmp("no-quit", longopts[li].name) == 0) {
				options.noQuit = true;
			}
			break;

		case 'f':
			if (sscanf(optarg, "%d", &options.frame) != 1) {
				LOG("ERROR: '%s' is not a valid frame number.", optarg);
				return false;
			}
			break;

		case 't':
			if (sscanf(optarg, "%f", &options.timestep) != 1) {
				LOG("ERROR: Timestep argument '%s' is not a valid decimal.", optarg);
				return false;
			}
			break;

		case 'T':
			if (sscanf(optarg, "%f", &options.intensityThreshold) != 1) {
				LOG("ERROR: Threshold argument '%s' is not a valid decimal.", optarg);
				return false;
			}
			break;

		case 'S':
			if (sscanf(optarg, "%f", &options.springScale) != 1) {
				LOG("ERROR: Spring scale argument '%s' is not a valid decimal.", optarg);
				return false;
			}
			break;

		case 'D':
			if (sscanf(optarg, "%f", &options.damperScale) != 1) {
				LOG("ERROR: Damper scale argument '%s' is not a valid decimal.", optarg);
				return false;
			}
			break;

		case 'L':
			if (sscanf(optarg, "%f", &options.timeLimit) != 1) {
				LOG("ERROR: Time limit argument '%s' is not a valid decimal.", optarg);
				return false;
			}
			break;

		case 'X':
			options.detectExplosion = true;
			break;

		case 'l':
			if (sscanf(optarg, "%d", &options.nLinks) != 1) {
				LOG("ERROR: Links argument '%s' is not a valid number of links.", optarg);
				return false;
			}
			break;


		default:
			return false;
		}
	}

	if (optind < argc)
		options.filename = argv[optind];

	if (!options.useCube && (options.filename == NULL)) {
		LOG("ERROR: No data source specified.");
		return false;
	}

	return true;
}

void printUsage()
{
	static const char usageText[] = 
		"Usage: biosim [options] [file]\n"
		"Options:\n"
		"    --cube=WxHxD        Use a cube as demo data. Width, height and depth must be\n"
		"                        specified.\n"
		"    --noise=AMOUNT      Sets the amount of positional noise to apply to points.\n"
		"                        Default is 1.0.\n"
		"    -f N, --frame=N     Sets the frame to load from the data set. Default is\n"
		"                        frame 0.\n"
		"    -t SECONDS, --timestep=SECONDS\n"
		"                        Sets the timestep in seconds.\n"
		"    -T INTENSITY, --threshold=INTENSITY\n"
		"                        Sets the point discard threshold.\n"
		"    --detect-explosion  Detects quits if an explosion is detected.\n"
		"    -S, --spring-scale K\n"
		"                        Sets global scaling of the spring constants.\n"
		"    -D, --damper-scale D\n"
		"                        Sets the global scaling of the damper coefficients.\n"
		"    -L, --time-limit T  Sets a time limit for the simulation after which the\n"
		"                        program quits.\n"
		"    --save FILENAME     Save linked point to file. Can be specified as source\n"
		"                        file to load. Extension should be .msd\n"
		"    -l, --nlinks LINKS  The number of links to generate for image files.\n"
		"    --benchmark N       Run N steps and print benchmark stats.\n"
		"    --downsample N      Downsample image data N times.\n"
		"    --link-radius R     The maximum distance between points to be linked.\n"
		"    --test-force        Enables a fixed test force.\n"
		"    --no-realtime       Runs the simulation as fast as possible\n"
		"    --no-quit           Disables automatic termination on explosion or time\n"
		"                        limit.\n";
	printf("%s", usageText);
}

bool initialize()
{
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 3);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 2);
	glfwOpenWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	int error = glfwOpenWindow(1024, 768, 0, 0, 0, 0, 0, 0, GLFW_WINDOW);
	if (error != GL_TRUE) {
		fprintf(stderr, "ERROR: Unable to open window.\n");
		return false;
	}

#ifndef __APPLE__
	glewExperimental = GL_TRUE;
	GLenum glerror = glewInit();
	if (glerror != GLEW_OK) {
		LOG("ERROR: GLEW initialization failed: %s", glewGetErrorString(glerror));
		return false;
	}
#endif

	clContext = createClContext();
	if (clContext == NULL) {
		LOG("ERROR: Unable to create OpenCL context.");
		return false;
	}

	simulator = new Simulator();
	simulator->setTimestep(options.timestep);
	simulator->setSpringScale(options.springScale);
	simulator->setDamperScale(options.damperScale);
	simulator->setTestForce(options.testForce);

	PointData *data = createData();
	if (data == NULL)
		return false;
	bool simulatorInit = simulator->initialize(clContext, data);
	delete data;
	if (!simulatorInit)
		return false;

	renderer = new Renderer();
	if (!renderer->initialize(simulator))
		return false;

	glfwSetWindowSizeCallback(windowResized);
	glfwSetMouseButtonCallback(mouseButtonChanged);
	glfwSetMousePosCallback(mouseMoved);
	glfwSetMouseWheelCallback(mouseWheelMoved);
	glfwSetKeyCallback(keyChanged);
	
	int width, height;
	glfwGetWindowSize(&width, &height);
	windowResized(width, height);
	glfwGetMousePos(&mouseLastPosX, &mouseLastPosY);
	mouseWheelLastPos = glfwGetMouseWheel();

	return true;
}

PointData *createData()
{
	PointData *data = NULL;
	if (options.useCube) {
		VoxelImage image(options.cubeWidth, options.cubeHeight, options.cubeDepth, 1, NULL);
		float one = 1.0f;
		image.fill(&one);
		data = new PointData(&image, options.nLinks);
	} else {
		//If this is an .msd file, treat it as prelinked point data.
		const char *dotp = strrchr(options.filename, '.');
		if ((dotp != NULL) && (strcmp(dotp + 1, "msd") == 0)) {
			data = PointData::loadFromFile(options.filename);
			if (data == NULL)
				LOG("ERROR: Unable to load file %s", options.filename);
			return data;
		} else {
			DataSet *dataSet = DataSet::readFromFile(options.filename);
			if (dataSet == NULL) {
				LOG("ERROR: Unable to load file %s", options.filename);
				return NULL;
			}
			
			VoxelImage *image = dataSet->frame(options.frame);
			LOG("INFO: Image dimensions: %d x %d x %d", image->width(), image->height(), image->depth());
			if (options.downsample > 0) {
				LOG("INFO: Downsampling...");
				VoxelImage *downsampled = image->downsampled(options.downsample);
				data = new PointData(downsampled, options.nLinks, options.intensityThreshold);
				delete downsampled;
			} else {
				data = new PointData(image, options.nLinks, options.intensityThreshold);
			}
			delete dataSet;
		}
	}

	LOG("INFO: %d points remaining.", data->nPoints());
	data->scramble(glm::vec3(options.noise));

	LOG("INFO: Generating links...");
	data->autoLink(options.linkRadius);
	std::vector<int> histogram = data->linkageHistogram();
	int totalLinks = 0;
	for (int i = 0; i < histogram.size(); i++) {
		totalLinks += (i * histogram[i]);
		LOG("INFO: Linkage = %d: %d", i, histogram[i]);
	}
	LOG("INFO: %d links in total.", totalLinks / 2);

	if (options.saveFile != NULL) {
		LOG("INFO: Saving point data to %s", options.saveFile);
		if (!data->saveToFile(options.saveFile))
			LOG("WARNING: Unable to save data.");
	}

	return data;
}

void run()
{
	glfwSetTime(0.0f);
	double lastTime = 0.0f;
	bool simulationDone = false;

	while (!simulationDone || options.noQuit) {
		double now = glfwGetTime();
		if (!simulationDone && !simulationPaused) {
			float dt = now - lastTime;
			if (options.realtime) {
				simulator->advanceSimulation(dt);
				if (((simulator->nSteps() % 20) == 0) && (simulator->avgTimeStep() > simulator->timestep())) {
					LOG("WARNING: Average step time is larger than timestep (%f > %f).",
						simulator->avgTimeStep(),
						simulator->timestep());
				}
			} else {
				simulator->step();
			}
		}

		if (options.detectExplosion && simulator->hasExploded()) {
			LOG("INFO: Explosion detected!");
			simulationDone = true;
		}

		if (!simulationDone && (simulator->time() > options.timeLimit)) {
			LOG("INFO: Time limit reached.");
			simulationDone = true;
		}
		
		updateTitle();
		renderer->draw();
		glfwSwapBuffers();
		lastTime = now;
	}
}

void runBenchmark()
{
	glfwSetTime(0.0f);
	for (int i = 0; i < options.benchmark; i++)
		simulator->step();
	double t = glfwGetTime();

	printf("nsteps=%d time=%f\n", options.benchmark, t);
}

void updateTitle()
{
	const char *toolName;
	if (currentTool == TOOL_MOVE)
		toolName = "Move";
	else if(currentTool == TOOL_CUT)
		toolName = "Cut";
	else
		toolName = "No";

	float stepTime = simulator->avgTimeStep();
	float fps = 1.0f / stepTime;

	char title[256];
	snprintf(title, sizeof(title), "Step time: %1.4f s | Limit: %3.1f fps | %s tool", stepTime, fps, toolName);
	glfwSetWindowTitle(title);
}

void updateSelection()
{
	int x, y;
	glfwGetMousePos(&x, &y);
	renderer->coneSelect(toCameraSpace(x, y), 0.01f, selectionRadius);
}

glm::vec2 toCameraSpace(int x, int y)
{
	int width, height;
	glfwGetWindowSize(&width, &height);
	return glm::vec2(x, height - y);
}

void cleanup()
{
	delete renderer;
	renderer = NULL;
	delete simulator;
	simulator = NULL;
	clReleaseContext(clContext);
}

void GLFWCALL windowResized(int width, int height)
{
	renderer->reshape(width, height);
}

void GLFWCALL mouseButtonChanged(int button, int action)
{
	int x, y;
	glfwGetMousePos(&x, &y);

	mouseDrag = (action == GLFW_PRESS);

	if (action == GLFW_PRESS) {
		mouseDragButton = button;
		mouseDragStartX = x;
		mouseDragStartY = y;

		if ((currentTool == TOOL_MOVE) && (button == GLFW_MOUSE_BUTTON_LEFT))
			updateSelection();
	}

	if (rebuildLinks) {
		renderer->rebuildLinks();
		rebuildLinks = false;
	}
}

void GLFWCALL mouseMoved(int x, int y)
{
	int dx = x - mouseLastPosX;
	int dy = y - mouseLastPosY;
	mouseLastPosX = x;
	mouseLastPosY = y;

	if (mouseDrag)
		mouseDragged(x, y, dx, dy);
}

void GLFWCALL mouseWheelMoved(int pos)
{
	int dp = pos - mouseWheelLastPos;
	float scale = fmax(renderer->scale() + (dp * 0.0025f), 0.0f);
	renderer->setScale(scale);
	mouseWheelLastPos = pos;
}

void GLFWCALL keyChanged(int key, int action)
{
	if (action == GLFW_PRESS) {
		switch (key) {
		case '1':
			currentTool = TOOL_MOVE;
			break;

		case '2':
			currentTool = TOOL_CUT;
			break;

		case 'S':
			if (currentTool == TOOL_MOVE) {
				selectionRadius += 1.0f;
			}
			break;

		case 'A':
			if (currentTool == TOOL_MOVE) {
				selectionRadius = fmaxf(0.0f, selectionRadius - 1.0f);
			}
			break;

		case 'L':
			renderer->setDrawLinks(!renderer->drawLinks());
			break;

		case GLFW_KEY_SPACE:
			simulationPaused = !simulationPaused;
			break;
		}
	}
}

void mouseDragged(int x, int y, int dx, int dy)
{
	if (mouseDragButton == GLFW_MOUSE_BUTTON_LEFT) {
		if (currentTool == TOOL_MOVE) {
			renderer->moveSelection(toCameraSpace(x, y));
		} else if(currentTool == TOOL_CUT) {
			renderer->coneCut(toCameraSpace(x, y), 0.02f);
			rebuildLinks = true;
		}
	} else if (mouseDragButton == GLFW_MOUSE_BUTTON_RIGHT) {
		if (glfwGetKey(GLFW_KEY_LSHIFT) == GLFW_PRESS) {
			int width, height;
			glfwGetWindowSize(&width, &height);
			glm::vec2 offset = glm::vec2((float)dx, (float)-dy) / (float)height;
			renderer->setPan(renderer->pan() + offset);
		} else {
			renderer->setRotationX(renderer->rotationX() + dy);
			renderer->setRotationY(renderer->rotationY() + dx);
		}
	}
}

void errorCallback(const char *errInfo, const void *privateInfo, size_t cb, void *userData)
{
	LOG("ERROR: OpenCL error: %s", errInfo);
}

cl_context createClContext()
{
	int nDevices = 0;
	cl_device_id *devices = NULL;

#ifdef __APPLE__
	//Context for Mac OS X for OpenGL sharing is super simple.
	CGLContextObj glContext = CGLGetCurrentContext();
	cl_context_properties contextProperties[] = {
		CL_CONTEXT_PROPERTY_USE_CGL_SHAREGROUP_APPLE, (cl_context_properties)CGLGetShareGroup(glContext), 0
	};
#else /* __APPLE__ */

	const int MAX_PLATFORMS = 8;
	cl_platform_id platforms[MAX_PLATFORMS];
	cl_uint numPlatforms;
	cl_int err;
	err = clGetPlatformIDs(MAX_PLATFORMS, platforms, &numPlatforms);
	if (err != CL_SUCCESS) {
	  LOG("ERROR: Failed to identify OpenCL platforms. Problem with OpenCL installation/drivers?");
	  return NULL;
	}

	LOG("INFO: Number of platforms: %d", numPlatforms);
	if (numPlatforms > 1) {
		LOG("WARNING: We have more (%d) than one platform. We will use just the first one for GL sharing - "
	  		"but this may be incorrect!", numPlatforms);
	}
	cl_platform_id platform = platforms[0];

	cl_context_properties contextProperties[] = {
		CL_CONTEXT_PLATFORM, (cl_context_properties)platform,
#ifdef _WIN32
		CL_GL_CONTEXT_KHR, (cl_context_properties)wglGetCurrentContext(),
		CL_WGL_HDC_KHR, (cl_context_properties)wglGetCurrentDC(), 0
#else
		CL_GL_CONTEXT_KHR, (cl_context_properties)glXGetCurrentContext(),
		CL_GLX_DISPLAY_KHR, (cl_context_properties)glXGetCurrentDisplay(), 0
#endif
	};	

	clGetGLContextInfoKHR_fn clGetGLContextInfoKHR;
	clGetGLContextInfoKHR = (clGetGLContextInfoKHR_fn)clGetExtensionFunctionAddressForPlatform(platform, "clGetGLContextInfoKHR");
	if(!clGetGLContextInfoKHR) {
		LOG("ERROR: Failed to find extension 'clGetGLContextInfoKHR'");
		return NULL;
	}

	cl_device_id device;
	// We use this to find out what device is associated with the OpenGL context. This is
	// the device that must be used for the OpenCL context for CL/GL sharing to work.
	// Context properties are used here to identify a specific OpenGL context
	// by the context handle, the platform display handle and the OpenCL platform
	cl_int error = clGetGLContextInfoKHR(
		contextProperties,
		CL_CURRENT_DEVICE_FOR_GL_CONTEXT_KHR,
		sizeof(cl_device_id), &device, NULL);
	if (error != CL_SUCCESS) {
		LOG("ERROR: Unable to get OpenCL device from OpenGL context.");
		return NULL;
	}
	devices = &device;
	nDevices = 1;
#endif

	return clCreateContext(contextProperties, nDevices, devices, errorCallback, NULL, NULL);
}

int main(int argc, char **argv)
{
	bool exploded = false;
	if (parseOptions(argc, argv)) {
		glfwInit();
		if (initialize()) {
			if (options.benchmark > 0)
				runBenchmark();
			else
				run();
			exploded = simulator->hasExploded();
		}
		cleanup();
		//glfwTerminate();
	} else {
		printUsage();
	}

	return exploded ? 1 : 0;
}
