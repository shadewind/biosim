#include "DataSet.h"

#include <cstdio>
#include <stdint.h>
#include <arpa/inet.h>

#include "VoxelImage.h"

#include "log.h"

DataSet::DataSet(int nFrames, VoxelImage **frames) : _nFrames(nFrames), _frames(frames)
{
	
}

int DataSet::nFrames() const
{
	return _nFrames;
}

VoxelImage *DataSet::frame(int frame) const
{
	return _frames[frame];
}

DataSet *DataSet::readFromFile(const char *filename)
{
	FILE *file = fopen(filename, "rb");
	if (file == NULL)
		return NULL;
	
	uint16_t version;
	uint16_t width, height, depth, nFrames, elementSize;
	fread(&version, sizeof(uint16_t), 1, file);
	version = htons(version);
	if (version != 1) {
		fclose(file);
		return NULL;
	}
	
	fread(&width, sizeof(uint16_t), 1, file);
	width = ntohs(width);
	fread(&height, sizeof(uint16_t), 1, file);
	height = ntohs(height);
	fread(&depth, sizeof(uint16_t), 1, file);
	depth = ntohs(depth);
	fread(&nFrames, sizeof(uint16_t), 1, file);
	nFrames = ntohs(nFrames);
	fread(&elementSize, sizeof(uint16_t), 1, file);
	elementSize = ntohs(elementSize);
	
	int frameSize = width * height * depth * elementSize;
	VoxelImage **frames = new VoxelImage *[nFrames];
	for (int i = 0; i < nFrames; i++) {
		float *frameData = new float[frameSize];
		fread(frameData, sizeof(float), frameSize, file);
		frames[i] = new VoxelImage(width, height, depth, elementSize, frameData);
	}
	
	fclose(file);
	return new DataSet(nFrames, frames);
}

DataSet::~DataSet()
{
	delete[] _frames;
}