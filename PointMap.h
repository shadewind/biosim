#ifndef __BioSim__PointMap__
#define __BioSim__PointMap__

#include <vector>

#include <glm/glm.hpp>

class PointData;

/**
 * Divides PointData as a grid to enable faster searches by position.
 */
class PointMap {
public:
	/**
	 * Constructs a new \c PointMap for the specified point data. The extents
	 * of the map will be determined automatically.
	 *
	 * @param data      The \c PointData object.
	 * @param binSize   The size of each bin.
	 */
	PointMap(const PointData *data, float binSize);

	/**
	 * Returns the bin index for the specified position.
	 *
	 * @param pos  The position.
	 *
	 * @return The index.
	 */
	glm::ivec3 indexFor(const glm::vec4 &pos) const;

	/**
	 * Returns the bin for the specified index.
	 *
	 * @param index  The index.
	 *
	 * @return A reference to the bin.
	 */
	const std::vector<int> &binFor(const glm::ivec3 &index) const;

	/**
	 * Returns the bin for the specified position.
	 *
	 * @param pos  The position.
	 *
	 * @return A reference to the bin.
	 */
	const std::vector<int> &binFor(const glm::vec4 &pos) const;

	/**
	 * Returns the number of bins for all dimensions as a vectors.
	 *
	 * @return The number of bins.
	 */
	glm::ivec3 nBins() const;

	/**
	 * Returns the bin size.
	 *
	 * @return The bin size.
	 */
	float binSize() const;

private:
	const PointData *_data;
	std::vector<std::vector<int> > _bins;
	float _binSize;
	glm::vec3 _origin;
	glm::vec3 _size;
	glm::ivec3 _nBins;
};

#endif /* defined(__BioSim__PointMap__) */
